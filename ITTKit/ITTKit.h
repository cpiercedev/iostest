//
//  ITTKit.h
//  ITTKit
//
//  Created by Conor Pierce on 3/1/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ITTKit.
FOUNDATION_EXPORT double ITTKitVersionNumber;

//! Project version string for ITTKit.
FOUNDATION_EXPORT const unsigned char ITTKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ITTKit/PublicHeader.h>


