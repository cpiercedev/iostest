//
//  Font.swift
//  SwiftEntryKit_Example
//
//  Created by Daniel Huri on 4/23/18.
//  Copyright (c) 2018 huri000@gmail.com. All rights reserved.
//

import UIKit

typealias MainFont = Font.SFProDisplay

enum Font {
    enum SFProDisplay: String {
        case ultraLightItalic = "UltraLightItalic"
        case medium = "Medium"
        case mediumItalic = "MediumItalic"
        case ultraLight = "UltraLight"
        case italic = "Italic"
        case light = "Light"
        case thinItalic = "ThinItalic"
        case lightItalic = "LightItalic"
        case bold = "Bold"
        case thin = "Thin"
        case condensedBlack = "CondensedBlack"
        case condensedBold = "CondensedBold"
        case boldItalic = "BoldItalic"
        
        
        func with(size: CGFloat) -> UIFont {
            for family in UIFont.familyNames.sorted() {
                let names = UIFont.fontNames(forFamilyName: family)
               //print("Family: \(family) Font names: \(names)")
            }
            //print(UIFont(name: "SF-Pro-Display-Thin.otf", size: 12))
            return UIFont(name: "SFProDisplay-Thin", size: size)!
        }
    }
}
