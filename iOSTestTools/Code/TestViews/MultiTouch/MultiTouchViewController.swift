//
//  MultiTouchViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/7/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer //Only for hidding  Volume view
import SwiftEntryKit

class MultiTouchViewController: UIViewController, segueProtocol {
    
    
    let testName = "MultiTouch"
    
    @IBOutlet var TouchableView: TouchableView!
    
    
    var seguePath = "SegueToGrid"
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 0))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        TouchableView.delegate = self
        TouchableView.backgroundColor = .white
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        let font = MainFont.light.with(size: 12)
        let titleText = "Unable to perform a test?"
        let descText = "Manually mark the test result by pressing a volume button on most tests."
        let titleFont = MainFont.light.with(size: 24)
        let descFont = MainFont.light.with(size: 12)
        let textColor = UIColor.black
        
        let imageName = "nfc_3834.png"
        
        
        var attributes: EKAttributes
        var descriptionString: String
        var descriptionThumb: String
        
        attributes = .topNote
        attributes.hapticFeedbackType = .success
        //attributes.entryBackground = .gradient(gradient: .init(colors: [EKColor.BlueGradient.dark, EKColor.BlueGradient.light], startPoint: .zero, endPoint: CGPoint(x: 1, y: 1)))
        attributes.entryInteraction = .delayExit(by: 3)
        attributes.displayDuration = 5
        attributes.entryBackground = .visualEffect(style: .standard)
        attributes.screenInteraction = .dismiss
        attributes.entryInteraction = .absorbTouches
        attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
        attributes.statusBar = .dark
        attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
        descriptionString = "Bottom float with gradient background. Touches delay exit"
        //var description = .init(with: attributes, title: "Bottom", description: descriptionString, thumb: descriptionThumb)
        
        
        
        let title = EKProperty.LabelContent(text: titleText, style: .init(font: titleFont, color: .standardContent))
        var description = EKProperty.LabelContent(text: descText, style: .init(font: descFont, color: .standardContent))
        let image = EKProperty.ImageContent(image: UIImage(named: imageName)!, size: CGSize(width: 35, height: 35))
        let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
        let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
        
        let contentView = EKNotificationMessageView(with: notificationMessage)
        let userDefaults = UserDefaults.standard
        var storeNumber = userDefaults.integer(forKey: "StoreNumber")
        
        
        if(storeNumber == 9999){
            SwiftEntryKit.display(entry: contentView, using: attributes)
        }
    }
    weak var delegate: segueProtocol?
    func segue(){
        
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("Pass", forKey: "MultiTouch")
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
        
    }
    
    
    //myImage.transform = CGAffineTransform(scaleX: sender.scale, y: sender.scale)
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        SwiftEntryKit.dismiss()
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    if SwiftEntryKit.isCurrentlyDisplaying {
                        return
                    }
                    var attributes: EKAttributes
                    var imageName = "SIMFailed.png"
                    attributes = .bottomFloat
                    attributes.hapticFeedbackType = .success
                    attributes.screenInteraction = .dismiss
                    attributes.entryInteraction = .absorbTouches
                    attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
                    attributes.screenBackground = .color(color: .standardContent)
                    attributes.entryBackground = .color(color: .white)
                    attributes.entranceAnimation = .init(translate: .init(duration: 0.7, spring: .init(damping: 1, initialVelocity: 0)), scale: .init(from: 0.6, to: 1, duration: 0.7), fade: .init(from: 0.8, to: 1, duration: 0.3))
                     attributes.exitAnimation = .init(scale: .init(from: 1, to: 0.7, duration: 0.3), fade: .init(from: 1, to: 0, duration: 0.3))
                    attributes.displayDuration = .infinity
                    attributes.border = .value(color: .black, width: 0.5)
                    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 5))
                    attributes.statusBar = .dark
                    attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
                    //descriptionString = "Top floating alert view with button bar. Smooths in animately."
                    
                    let title = EKProperty.LabelContent(text: "Mark test manually", style: .init(font: MainFont.medium.with(size: 15), color: .black))
                    let description = EKProperty.LabelContent(text: "If you can't complete the test, you can mark it manually.", style: .init(font: MainFont.light.with(size: 13), color: .black))
                    let image = EKProperty.ImageContent(imageName: imageName, size: CGSize(width: 35, height: 35), contentMode: .scaleAspectFit)
                    let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
                    
                    // Generate buttons content
                    let buttonFont = MainFont.medium.with(size: 16)
                    
                    // Close button - Just dismiss entry when the button is tapped
                    let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let closeButtonLabel = EKProperty.LabelContent(text: "Not Available", style: closeButtonLabelStyle)
                    let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) {
                        self.markNA()
                        SwiftEntryKit.dismiss()
                    }
                    
                    
                    // Ok Button - Make transition to a new entry when the button is tapped
                    let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let okButtonLabel = EKProperty.LabelContent(text: "Failed", style: okButtonLabelStyle)
                    let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) { [unowned self] in
                        //var attributes = self.dataSource.bottomAlertAttributes
                        self.markFail()
                        SwiftEntryKit.dismiss()
                        
                    }
                    
                    let buttonsBarContent = EKProperty.ButtonBarContent(with: closeButton, okButton, separatorColor: .standardContent, buttonHeight: 60, expandAnimatedly: false)
                    
                    // Generate the content
                    let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, imagePosition: .left, buttonBarContent: buttonsBarContent)
                    
                    let contentView = EKAlertMessageView(with: alertMessage)
                    
                    SwiftEntryKit.display(entry: contentView, using: attributes)
                    
                    
//                    print("Volume Button Pressed")
//                    let alert = UIAlertController(title: "Select test status for MultiTouch", message: "", preferredStyle: .alert)
//
//                    alert.addAction(UIAlertAction(title: "N/A", style: .default, handler: { action in
//                        self.markNA()
//                    } ))
//                    alert.addAction(UIAlertAction(title: "Fail", style: .default, handler: { action in
//                        self.markFail()
//                    } ))
//                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
//                    self.present(alert, animated: true)
                }
            }
        }
    }
    func markNA(){
        sendNA(NAItem: testName)
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: "MultiTouch")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markFail(){
        sendFail(FailItem: testName)
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: "MultiTouch")
        sendFail(FailItem: "MultiTouch")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
}
