//
//  WRTViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 10/6/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import UIKit
import CoreLocation
import CoreMotion
import Charts
import MediaPlayer
import SwiftEntryKit

class WRTViewController: UIViewController {
    
    
    var testName = "WRT"
    
    
    @IBOutlet var barChartView: BarChartView!
    @IBOutlet var relativeAltitude: UILabel!
    @IBOutlet var testLabel: UILabel!
    @IBOutlet var getReferenceButton: UIButton!
    
    @IBOutlet var pressure: UILabel!
    var seguePath = "SegueToMulti"
    var minPressure = 0;
    var maxAltitude:Double = 0;
    var altitude: Double = 0.0;
    let altimeter = CMAltimeter()
    var testStarted = false
    var testFinished = false
    var testTimes = [Double]()
    var testValues = [Double]()
    var count = 0
    var timeoutCount = 0
    var startTime = Date()
    var elapsedTime:Double = 0
    var referenceAltitude:Double = 0.0
    var referenceCollected = false
    var finalAltitude:Double = 0
    var buttonPressed = false
    var testRunning = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
        let userDefaults = UserDefaults.standard
        let model = userDefaults.string(forKey: "model")
        let storeNumber = userDefaults.integer(forKey: "StoreNumber")
        if(storeNumber == 1){
            checkTestList()
        }
        else{
            userDefaults.set("N/A", forKey: "WRT")
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
        
        barChartView.chartDescription?.enabled = false
        barChartView.dragEnabled = false
        barChartView.setScaleEnabled(true)
        barChartView.pinchZoomEnabled = false
        
        
        let leftAxis = barChartView.leftAxis
        leftAxis.removeAllLimitLines()
        
        leftAxis.axisMaximum = 700
        leftAxis.axisMinimum = -50
        leftAxis.gridLineDashLengths = [1, 1]
        leftAxis.drawLimitLinesBehindDataEnabled = true
        
        let xAxis = barChartView.xAxis
        xAxis.axisMaximum = 20
        xAxis.axisMinimum = 0
        xAxis.labelFont = .systemFont(ofSize: 11)
        xAxis.labelTextColor = .black
        xAxis.drawAxisLineEnabled = true
        
        barChartView.legend.enabled = false
        barChartView.rightAxis.enabled = false
        updateChart()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
    }
    @IBAction func GetReferencePressed(_ sender: Any) {
        buttonPressed = true
        testRunning = true
        if CMAltimeter.isRelativeAltitudeAvailable() {
            altimeter.startRelativeAltitudeUpdates(to: OperationQueue.main) { (data, error) in
                
                if( self.testRunning){
                    self.relativeAltitude.text = String.init(format: "%.1fM",(data?.relativeAltitude.doubleValue)!)
                    //self.pressure.text = String.init(format: "%.2f hPA", (data?.pressure.floatValue)!*10)
                    
                    self.altitude = (data?.relativeAltitude.doubleValue)!
                    
                    //This is after the start button is pressed, before altitude has been checked.
                    if(!self.testStarted && !self.testFinished && self.buttonPressed){
                        self.testLabel.text = "Ready"
                        self.buttonPressed = false
                    }
                    //If the altitude has gone up, but only a little it could mean one of two things, either there wasn't enough force given, or there is a large leak.
                    if(self.altitude > 20 && self.altitude < 100){
                        self.timeoutCount += 1
                    }
                    
                    //Either not enough force or large leak.
                    if(self.timeoutCount > 4 && !self.testStarted){
                        self.testLabel.text = "Large leak detected, please check cover tapes"
                    }
                    if(!self.referenceCollected){
                        self.referenceAltitude = self.altitude
                    }
                    if(self.altitude > self.maxAltitude && !self.testStarted && !self.testFinished){
                        self.maxAltitude = self.altitude
                    }
                    else if(self.altitude > 350 && !self.testStarted && !self.testFinished){
                        self.testStarted = true
                        self.testLabel.text = "Test Started"
                        self.getReferenceButton.isEnabled = false
                        self.startTime = Date()
                    }
                    else if(self.count == 20 && !self.testFinished){
                        self.testFinished = true
                        self.finalAltitude = self.altitude
                        self.testLabel.text = "Test Finished"
                        self.testRunning = false
                        self.showData()
                        if(self.altitude > self.maxAltitude*0.95){
                            self.testLabel.text = "Passed"
                            let userDefaults = UserDefaults.standard
                            userDefaults.set("Pass", forKey: "WRT")
                            print("WRT Passed")
                            let singleTest = userDefaults.bool(forKey: "SingleTest")
                            
                            if(singleTest){
                                self.dismiss(animated: true, completion: nil)
                            }
                            else{
                                self.performSegue(withIdentifier: self.seguePath, sender: self)
                            }
                        }
                        else{
                            self.testLabel.text = "Failed"
                            self.getReferenceButton.isEnabled = true
                            self.testTimes = []
                            self.testValues = []
                            self.count = 0
                            self.timeoutCount = 0
                            self.testStarted = false
                            self.testFinished = false
                        }
                    }
                    else if(self.testStarted && !self.testFinished){
                        self.elapsedTime = Date().timeIntervalSince(self.startTime)
                        self.testTimes.append(self.elapsedTime)
                        self.testValues.append(self.altitude)
                        self.updateChart()
                        print(self.testValues)
                        self.count += 1
                        self.pressure.text = "\(self.count)"
                    }
                    
                }
            }
        }
    }
    func showData(){
        updateChart()
    }
    
    
    private func updateChart() {
        var chartEntry = [ChartDataEntry]()
        
        for i in 0..<testValues.count {
            let value = ChartDataEntry(x: testTimes[i], y: testValues[i])
            chartEntry.append(value)
        }
        
        let line = LineChartDataSet(entries: chartEntry, label: "Values")
        line.colors = [UIColor.red]
        line.lineWidth = 2
        line.circleRadius = 0
        line.highlightColor = .red
        line.drawCircleHoleEnabled = false
        line.drawValuesEnabled = false
        let data = LineChartData()
        data.addDataSet(line)
        
        barChartView.data = data
        barChartView.chartDescription?.text = "Seconds"
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            var force = touch.force/touch.maximumPossibleForce
            print(force)
        }
        
        
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    if SwiftEntryKit.isCurrentlyDisplaying {
                        return
                    }
                    var attributes: EKAttributes
                    var imageName = "SIMFailed.png"
                    attributes = .bottomFloat
                    attributes.hapticFeedbackType = .success
                    attributes.screenInteraction = .dismiss
                    attributes.entryInteraction = .absorbTouches
                    attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
                    attributes.screenBackground = .color(color: .standardContent)
                    attributes.entryBackground = .color(color: .white)
                    attributes.entranceAnimation = .init(translate: .init(duration: 0.7, spring: .init(damping: 1, initialVelocity: 0)), scale: .init(from: 0.6, to: 1, duration: 0.7), fade: .init(from: 0.8, to: 1, duration: 0.3))
                     attributes.exitAnimation = .init(scale: .init(from: 1, to: 0.7, duration: 0.3), fade: .init(from: 1, to: 0, duration: 0.3))
                    attributes.displayDuration = .infinity
                    attributes.border = .value(color: .black, width: 0.5)
                    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 5))
                    attributes.statusBar = .dark
                    attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
                    //descriptionString = "Top floating alert view with button bar. Smooths in animately."
                    
                    let title = EKProperty.LabelContent(text: "Mark test manually", style: .init(font: MainFont.medium.with(size: 15), color: .black))
                    let description = EKProperty.LabelContent(text: "If you can't complete the test, you can mark it manually.", style: .init(font: MainFont.light.with(size: 13), color: .black))
                    let image = EKProperty.ImageContent(imageName: imageName, size: CGSize(width: 35, height: 35), contentMode: .scaleAspectFit)
                    let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
                    
                    // Generate buttons content
                    let buttonFont = MainFont.medium.with(size: 16)
                    
                    // Close button - Just dismiss entry when the button is tapped
                    let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let closeButtonLabel = EKProperty.LabelContent(text: "Not Available", style: closeButtonLabelStyle)
                    let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) {
                        self.markNA()
                        SwiftEntryKit.dismiss()
                    }
                    
                    
                    // Ok Button - Make transition to a new entry when the button is tapped
                    let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let okButtonLabel = EKProperty.LabelContent(text: "Failed", style: okButtonLabelStyle)
                    let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) { [unowned self] in
                        //var attributes = self.dataSource.bottomAlertAttributes
                        self.markFail()
                        SwiftEntryKit.dismiss()
                        
                    }
                    let buttonsBarContent = EKProperty.ButtonBarContent(with: closeButton, okButton, separatorColor: .standardContent, buttonHeight: 60, expandAnimatedly: false)
                    
                    // Generate the content
                    let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, imagePosition: .left, buttonBarContent: buttonsBarContent)
                    
                    let contentView = EKAlertMessageView(with: alertMessage)
                    
                    SwiftEntryKit.display(entry: contentView, using: attributes)
                }
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        SwiftEntryKit.dismiss()
    }
    func markNA(){
        sendNA(NAItem: "WRT")
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: "WRT")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: "WRT")
        sendFail(FailItem: "WRT")
        
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
    
}
