//
//  CameraTest.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/8/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit

class CameraTest: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet var ImageView: UIImageView!
    
    
    var seguePath = "SegueToVibration"
    
    @IBOutlet var CameraLabel: UILabel!
    var imagePicker: UIImagePickerController!
    var imagePicker1: UIImagePickerController!
    var cameraLoaded = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if cameraLoaded == 0{
            cameraLoaded = 1
            
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.cameraDevice = .front
            present(imagePicker, animated: true, completion: nil)
            //imagePicker.takePicture()
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func FailButtonPressed(_ sender: Any) {
        if(cameraLoaded == 1){
            let userDefaults = UserDefaults.standard
            userDefaults.set("Fail", forKey: "Front Camera")
            imagePicker1 = UIImagePickerController()
            imagePicker1.delegate = self
            imagePicker1.sourceType = .camera
            imagePicker1.cameraDevice = .rear
            present(imagePicker1, animated: true, completion: nil)
           // imagePicker1.takePicture()
            cameraLoaded = 2
            CameraLabel.text = "Rear Camera"
        }
        if(cameraLoaded == 2){
            let userDefaults = UserDefaults.standard
            userDefaults.set("Fail", forKey: "Rear Camera")
            sendFail(FailItem: "RearCamera")
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
        
    }
    
    
    @IBAction func PassButtonPressed(_ sender: Any) {
        if(cameraLoaded == 2){
            let userDefaults = UserDefaults.standard
            userDefaults.set("Pass", forKey: "RearCamera")
            //let success = saveImage(image: ImageView.image!, name: "RearCamera")
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
        if(cameraLoaded == 1){
            let userDefaults = UserDefaults.standard
            userDefaults.set("Pass", forKey: "FrontCamera")
           // let success = saveImage(image: ImageView.image!, name: "FrontCamera")
            imagePicker1 = UIImagePickerController()
            imagePicker1.delegate = self
            imagePicker1.sourceType = .camera
            imagePicker1.cameraDevice = .rear
            //imagePicker1.showsCameraControls = false
            present(imagePicker1, animated: true, completion: nil)
            //imagePicker1.takePicture()
            cameraLoaded = 2
            CameraLabel.text = "Rear Camera"
        }
       
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
            ImageView.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func saveImage(image: UIImage, name: String) -> Bool {
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            try data.write(to: directory.appendingPathComponent("\(name).png")!)
            return true
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
