
import Foundation
import UIKit
import AVFoundation
import MediaPlayer
//import Fabric
//mport Crashlytics
import SwiftEntryKit

class HeadphoneLoopback: UIViewController {
    
    var testName = "Headphone"
    
    let engine = AVAudioEngine()
    var playerStarted = false
    var playerInitial = false
    var player2 = AVAudioPlayerNode()
    var headphones = false
    var currentTest = false
    
    @IBOutlet var PassButton: UIButton!
    
    //Setting the default sequential path
    var seguePath = "SegueToSpeaker"
    let volumeView = MPVolumeView(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clearTap()
        
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        view.addSubview(volumeView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
        if let view = volumeView.subviews.first as? UISlider{
            view.value = 0.8 //---0 t0 1.0---
            
        }
        

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
        currentTest = true
        clearTap()
        
        
        let userDefaults = UserDefaults.standard
        let model = userDefaults.string(forKey: "model")
        
        
        
        headphone()
        
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        SwiftEntryKit.dismiss()
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    if SwiftEntryKit.isCurrentlyDisplaying {
                        return
                    }
                    var attributes: EKAttributes
                    var imageName = "SIMFailed.png"
                    attributes = .bottomFloat
                    attributes.hapticFeedbackType = .success
                    attributes.screenInteraction = .dismiss
                    attributes.entryInteraction = .absorbTouches
                    attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
                    attributes.screenBackground = .color(color: .standardContent)
                    attributes.entryBackground = .color(color: .white)
                    attributes.entranceAnimation = .init(translate: .init(duration: 0.7, spring: .init(damping: 1, initialVelocity: 0)), scale: .init(from: 0.6, to: 1, duration: 0.7), fade: .init(from: 0.8, to: 1, duration: 0.3))
                     attributes.exitAnimation = .init(scale: .init(from: 1, to: 0.7, duration: 0.3), fade: .init(from: 1, to: 0, duration: 0.3))
                    attributes.displayDuration = .infinity
                    attributes.border = .value(color: .black, width: 0.5)
                    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 5))
                    attributes.statusBar = .dark
                    attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
                    //descriptionString = "Top floating alert view with button bar. Smooths in animately."
                    
                    let title = EKProperty.LabelContent(text: "Mark test manually", style: .init(font: MainFont.medium.with(size: 15), color: .black))
                    let description = EKProperty.LabelContent(text: "If you can't complete the test, you can mark it manually.", style: .init(font: MainFont.light.with(size: 13), color: .black))
                    let image = EKProperty.ImageContent(imageName: imageName, size: CGSize(width: 35, height: 35), contentMode: .scaleAspectFit)
                    let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
                    
                    // Generate buttons content
                    let buttonFont = MainFont.medium.with(size: 16)
                    
                    // Close button - Just dismiss entry when the button is tapped
                    let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let closeButtonLabel = EKProperty.LabelContent(text: "Not Available", style: closeButtonLabelStyle)
                    let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) {
                        self.markNA()
                        SwiftEntryKit.dismiss()
                    }
                    
                    
                    // Ok Button - Make transition to a new entry when the button is tapped
                    let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let okButtonLabel = EKProperty.LabelContent(text: "Failed", style: okButtonLabelStyle)
                    let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) { [unowned self] in
                        //var attributes = self.dataSource.bottomAlertAttributes
                        self.markFail()
                        SwiftEntryKit.dismiss()
                        
                    }
                    let buttonsBarContent = EKProperty.ButtonBarContent(with: closeButton, okButton, separatorColor: .standardContent, buttonHeight: 60, expandAnimatedly: false)
                    
                    // Generate the content
                    let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, imagePosition: .left, buttonBarContent: buttonsBarContent)
                    
                    let contentView = EKAlertMessageView(with: alertMessage)
                    
                    SwiftEntryKit.display(entry: contentView, using: attributes)
                }
            }
        }
    }
    
    func markNA(){
        let userDefaults = UserDefaults.standard
        currentTest = false
        clearTap()
        NotificationCenter.default.removeObserver(AVAudioSession.routeChangeNotification)
        NotificationCenter.default.removeObserver(self, name: AVAudioSession.routeChangeNotification, object: nil)
        userDefaults.set("N/A", forKey: "Headphone")
        sendNA(NAItem: "Headphone")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markFail(){
        let userDefaults = UserDefaults.standard
        currentTest = false
        clearTap()
        NotificationCenter.default.removeObserver(AVAudioSession.routeChangeNotification)
        NotificationCenter.default.removeObserver(self, name: AVAudioSession.routeChangeNotification, object: nil)
        userDefaults.set("Fail", forKey: "Headphone")
        sendFail(FailItem: "Headphone")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    @IBAction func FailButtonPressed(_ sender: Any) {
        markFail()
    }
    @IBAction func PassButtonPressed(_ sender: Any) {
        let userDefaults = UserDefaults.standard
        currentTest = false

        clearTap()
        NotificationCenter.default.removeObserver(self, name: AVAudioSession.routeChangeNotification, object: nil)
        userDefaults.set("Pass", forKey: "Headphone")
        
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: AVAudioSession.routeChangeNotification, object: nil)
    }
    func startLoopback(){
        
        clearTap()
        startTap()
    }
    func stopLoopback(){
        clearTap()
    }
    func headphone(){
        clearTap()
        
        let currentRoute = AVAudioSession.sharedInstance().currentRoute
        if currentRoute.outputs != nil {
            for description in currentRoute.outputs {
                if convertFromAVAudioSessionPort(description.portType) == convertFromAVAudioSessionPort(AVAudioSession.Port.headphones) {
                    
                    startLoopback()
                    
                    DispatchQueue.main.async{
                        self.PassButton.isEnabled = true
                        print("headphone plugged in")
                    }
                    //startStop()
                } else {
                    //print("headphone pulled out")
                   
                    DispatchQueue.main.async{
                        self.PassButton.isEnabled = false
                        print("headphone pulled out")
                    }
                    
                    stopLoopback()
                    //startStop()
                }
            }
        } else {
            print("requires connection to device")
        }
    }
    func clearTap(){
        print("Tap Cleared")
        //CLSLogv("%@", getVaList(["Tap Cleared"]))
        engine.stop()
        let input = engine.inputNode
        input.removeTap(onBus: 0)
    }
    func startTap(){
        print("Tap Started")
        //CLSLogv("%@", getVaList(["Tap Started"]))
        let input = engine.inputNode
        input.removeTap(onBus: 0)
        let player2 = AVAudioPlayerNode()
        engine.attach(player2)
        
        let bus = 0
        let inputFormat = input.inputFormat(forBus: 0)
        
        engine.connect(player2, to: engine.mainMixerNode, format: inputFormat)
        
        input.installTap(onBus: bus, bufferSize: 512, format: inputFormat) { (buffer, time) -> Void in
            player2.scheduleBuffer(buffer)
        }
        
        try! engine.start()
        player2.play()
    }
    @objc func handleRouteChange(_ notification: Notification) {
        //startStop()
        if(currentTest){
        guard
            let userInfo = notification.userInfo,
            let reasonRaw = userInfo[AVAudioSessionRouteChangeReasonKey] as? NSNumber,
            let reason = AVAudioSession.RouteChangeReason(rawValue: reasonRaw.uintValue)
            else { fatalError("Strange... could not get routeChange") }
        switch reason {
        case .oldDeviceUnavailable:
            print("oldDeviceUnavailable")
            //CLSLogv("%@", getVaList(["Headphone unavailable"]))
            DispatchQueue.main.async{
                self.PassButton.isEnabled = false
                print("headphone pulled out")
            }
            clearTap()
        case .newDeviceAvailable:
            print("headset/line plugged in")
             //CLSLogv("%@", getVaList(["Headphone Plugged in"]))
             DispatchQueue.main.async{
                self.PassButton.isEnabled = true
                //print("headphone plugged in")
             }
            startTap()
        case .routeConfigurationChange:
            print("headset pulled out")
            //CLSLogv("%@", getVaList(["Headphone unplugged"]))
            DispatchQueue.main.async{
                self.PassButton.isEnabled = false
                //print("headphone pulled out")
            }
            clearTap()
        case .categoryChange:
            print("Just category change")
        default:
            print("not handling reason")
        }
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
        else{
            DispatchQueue.main.async {
                let observer = NotificationCenter.default.addObserver(self, selector: #selector(self.handleRouteChange(_:)), name: AVAudioSession.routeChangeNotification, object: nil)
                print("Observer Started")
            }
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionPort(_ input: AVAudioSession.Port) -> String {
	return input.rawValue
}
