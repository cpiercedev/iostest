//
//  SpeakerViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/8/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox
import AVFoundation
import MediaPlayer
import SwiftEntryKit

var player: AVAudioPlayer?

class SpeakerViewController: UIViewController{
    var number = 0
    var failCount = 0
    var startPressed = false
    var sounds = [String]()
    @IBOutlet var startButton: UIButton!
    //Setting the default sequential path
    var seguePath = "SegueToRearCamera"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startButton.isEnabled = true
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        
        self.view.addSubview(volumeView)
        //MPVolumeView.setVolume(0.8)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
        sounds.append("bird")
        sounds.append("train")
        sounds.append("rain")
    }
    
    @IBAction func VibrationStartPressed(_ sender: Any) {
        startPressed = true
        startButton.isEnabled = false
        number = Int.random(in: 0..<3)
        playSound(sound: sounds[number])
        
        print("Button Pressed")
        
        
    }
    @IBAction func birdPressed(_ sender: Any) {
        if(startPressed){
            player?.stop()
            if number == 0{
                player?.stop()
                player = nil
                startButton.isEnabled = true
                let userDefaults = UserDefaults.standard
                userDefaults.set("Pass", forKey: "Speakers")
                segue()
            }
            else{
                startButton.isEnabled = true
                failCount = failCount + 1
                
            }
            if(failCount == 2){
               
                player?.stop()
                player = nil
                let userDefaults = UserDefaults.standard
                userDefaults.set("Fail", forKey: "Speakers")
                segue()
            }
            startPressed = false
        }
    }
    @IBAction func trainPressed(_ sender: Any) {
        if(startPressed){
            player?.stop()
            if number == 1{
                player?.stop()
                player = nil
                let userDefaults = UserDefaults.standard
                userDefaults.set("Pass", forKey: "Speakers")
                segue()
                
            }
            else{
                failCount = failCount + 1
                startButton.isEnabled = true
            }
            if(failCount == 2){
                player?.stop()
                player = nil
                let userDefaults = UserDefaults.standard
                userDefaults.set("Fail", forKey: "Speakers")
                segue()
            }
            startPressed = false
        }
    }
    @IBAction func rainPressed(_ sender: Any) {
        if(startPressed){
            player?.stop()
            player = nil
            if number == 2{
                player?.stop()
                do {
                    try AVAudioSession.sharedInstance().setActive(false)
                }
                catch let error {
                    print(error.localizedDescription)
                }
                let userDefaults = UserDefaults.standard
                userDefaults.set("Pass", forKey: "Speakers")
                segue()
            }
            else{
                startButton.isEnabled = true
                failCount = failCount + 1
            }
            if(failCount == 2){
                player?.stop()
                player = nil
                let userDefaults = UserDefaults.standard
                userDefaults.set("Fail", forKey: "Speakers")
                segue()
            }
            startPressed = false
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        //SwiftEntryKit.dismiss()
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    if SwiftEntryKit.isCurrentlyDisplaying (entryNamed: "Test") {
                        return
                    }
                    var attributes = EKAttributes()
                    var imageName = "SIMFailed.png"
                    attributes = .bottomFloat
                    attributes.hapticFeedbackType = .success
                    attributes.screenInteraction = .dismiss
                    attributes.entryInteraction = .absorbTouches
                    attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
                    attributes.screenBackground = .color(color: .standardContent)
                    attributes.entryBackground = .color(color: .white)
                    attributes.entranceAnimation = .init(translate: .init(duration: 0.7, spring: .init(damping: 1, initialVelocity: 0)), scale: .init(from: 0.6, to: 1, duration: 0.7), fade: .init(from: 0.8, to: 1, duration: 0.3))
                     attributes.exitAnimation = .init(scale: .init(from: 1, to: 0.7, duration: 0.3), fade: .init(from: 1, to: 0, duration: 0.3))
                    attributes.displayDuration = .infinity
                    attributes.border = .value(color: .black, width: 0.5)
                    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 5))
                    attributes.statusBar = .dark
                    attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
                    //descriptionString = "Top floating alert view with button bar. Smooths in animately."
                    
                    let title = EKProperty.LabelContent(text: "Mark test manually", style: .init(font: MainFont.medium.with(size: 15), color: .black))
                    let description = EKProperty.LabelContent(text: "If you can't complete the test, you can mark it manually.", style: .init(font: MainFont.light.with(size: 13), color: .black))
                    let image = EKProperty.ImageContent(imageName: imageName, size: CGSize(width: 35, height: 35), contentMode: .scaleAspectFit)
                    let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
                    
                    // Generate buttons content
                    let buttonFont = MainFont.medium.with(size: 16)
                    
                    // Close button - Just dismiss entry when the button is tapped
                    let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let closeButtonLabel = EKProperty.LabelContent(text: "Not Available", style: closeButtonLabelStyle)
                    let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) {
                        self.markNA()
                        SwiftEntryKit.dismiss()
                    }
                    
                    
                    // Ok Button - Make transition to a new entry when the button is tapped
                    let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let okButtonLabel = EKProperty.LabelContent(text: "Failed", style: okButtonLabelStyle)
                    let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) { [unowned self] in
                        //var attributes = self.dataSource.bottomAlertAttributes
                        self.markFail()
                        SwiftEntryKit.dismiss()
                        
                    }
                    let buttonsBarContent = EKProperty.ButtonBarContent(with: closeButton, okButton, separatorColor: .standardContent, buttonHeight: 60, expandAnimatedly: false)
                    
                    // Generate the content
                    let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, imagePosition: .left, buttonBarContent: buttonsBarContent)
                    
                    let contentView = EKAlertMessageView(with: alertMessage)
                    
                    SwiftEntryKit.display(entry: contentView, using: attributes)
                }
            }
        }
    }
    func segue(){
         let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        //let storeNumber = userDefaults.integer(forKey: "StoreNumber")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
            
        }
    }
    func markNA(){
        sendNA(NAItem: "Speakers")
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: "Speakers")
        segue()
    }
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        sendFail(FailItem: "Speakers")
        userDefaults.set("Fail", forKey: "Speakers")
       segue()
    }
}
func playSound(sound: String) {
    guard let url = Bundle.main.url(forResource: sound, withExtension: "wav") else { return }
    
    do {
        try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)), mode: AVAudioSession.Mode.default)
        try AVAudioSession.sharedInstance().setActive(true)
        
        
        
        /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
        player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
        
        /* iOS 10 and earlier require the following line:
         player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
        
        guard let player = player else { return }
        
        player.play()
        
    } catch let error {
        print(error.localizedDescription)
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
