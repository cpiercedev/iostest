//
//  AmbientLightViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 10/6/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import UIKit
import MediaPlayer
import Foundation
import SwiftEntryKit
import Lottie

class AmbientLightViewController: UIViewController {
    
    var testName = "Ambient"
    
    var count = 0
    let seguePath = "SegueToGyro"
    var lastBrightness: CGFloat = 0.0;
    var testFinished = false;
    @IBOutlet var AmbientLabel: UILabel!
    @IBOutlet var ProgressBar: UIProgressView!
    @IBOutlet var AmbientAnimation: AnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
       
        AmbientLabel.text = String(format: "%0.3f%",UIScreen.main.brightness)
        
        NotificationCenter.default.addObserver(self, selector: #selector(brightnessChanged), name: UIScreen.brightnessDidChangeNotification, object: nil)
         UIScreen.main.brightness = CGFloat(0.4)
        ProgressBar.setProgress(0.0, animated: true)
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
        UIScreen.main.brightness = CGFloat(0.3)
        AmbientAnimation.loopMode = .loop
        AmbientAnimation.play()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        SwiftEntryKit.dismiss()
    }
    
    @objc func brightnessChanged(){
//        if(UIScreen.main.brightness >= CGFloat(0.9)){
//            UIScreen.main.brightness = CGFloat(0.4)
//            print("Manually Changed Brightness")
//        }
        let currentBrightness = UIScreen.main.brightness
        print(currentBrightness)
        AmbientLabel.text = String(format: "%0.3f%",currentBrightness)
        if(currentBrightness >= lastBrightness){
        count += 1
            print(count)
            updateProgress()
        }
        else{
            print(count)
            count += 1
            updateProgress()
        }
        lastBrightness = currentBrightness
        if(count >= 50){
            let userDefaults = UserDefaults.standard
            userDefaults.set("Pass", forKey: testName)
            UIDevice.current.isProximityMonitoringEnabled = false
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                 NotificationCenter.default.removeObserver(UIScreen.brightnessDidChangeNotification)
                dismiss(animated: true, completion: nil)
            }
            else{
                if(!testFinished){
                    testFinished = true
                 NotificationCenter.default.removeObserver(UIScreen.brightnessDidChangeNotification)
                performSegue(withIdentifier: seguePath, sender: self)
                }
            }
        }
    }
    func updateProgress (){
        let progression = Float(count)/100.0
        ProgressBar.setProgress(progression, animated: true)
        //print(progression)
    }
   
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    var attributes: EKAttributes
                    var imageName = "SIMFailed.png"
                    attributes = .bottomFloat
                    attributes.hapticFeedbackType = .success
                    attributes.screenInteraction = .dismiss
                    attributes.entryInteraction = .absorbTouches
                    attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
                    attributes.screenBackground = .color(color: .standardContent)
                    attributes.entryBackground = .color(color: .white)
                    attributes.entranceAnimation = .init(translate: .init(duration: 0.7, spring: .init(damping: 1, initialVelocity: 0)), scale: .init(from: 0.6, to: 1, duration: 0.7), fade: .init(from: 0.8, to: 1, duration: 0.3))
                    attributes.exitAnimation = .init(scale: .init(from: 1, to: 0.7, duration: 0.3), fade: .init(from: 1, to: 0, duration: 0.3))
                    attributes.displayDuration = .infinity
                    attributes.border = .value(color: .black, width: 0.5)
                    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 5))
                    attributes.statusBar = .dark
                    attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
                    //descriptionString = "Top floating alert view with button bar. Smooths in animately."
                    
                    let title = EKProperty.LabelContent(text: "Mark test manually", style: .init(font: MainFont.medium.with(size: 15), color: .black))
                    let description = EKProperty.LabelContent(text: "If you can't complete the test, you can mark it manually.", style: .init(font: MainFont.light.with(size: 13), color: .black))
                    let image = EKProperty.ImageContent(imageName: imageName, size: CGSize(width: 35, height: 35), contentMode: .scaleAspectFit)
                    let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
                    
                    // Generate buttons content
                    let buttonFont = MainFont.medium.with(size: 16)
                    
                    // Close button - Just dismiss entry when the button is tapped
                    let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let closeButtonLabel = EKProperty.LabelContent(text: "Not Available", style: closeButtonLabelStyle)
                    let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) {
                        self.markNA()
                        SwiftEntryKit.dismiss()
                    }
                    
                    
                    // Ok Button - Make transition to a new entry when the button is tapped
                    let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let okButtonLabel = EKProperty.LabelContent(text: "Failed", style: okButtonLabelStyle)
                    let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) { [unowned self] in
                        //var attributes = self.dataSource.bottomAlertAttributes
                        self.markFail()
                        SwiftEntryKit.dismiss()
                        
                    }
                    let buttonsBarContent = EKProperty.ButtonBarContent(with: okButton, separatorColor: .standardContent, buttonHeight: 60, expandAnimatedly: true)
                    
                    // Generate the content
                    let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, imagePosition: .left, buttonBarContent: buttonsBarContent)
                    
                    let contentView = EKAlertMessageView(with: alertMessage)
                    
                    SwiftEntryKit.display(entry: contentView, using: attributes)
                }
            }
        }
    }
    func markNA(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: "Ambient")
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        sendNA(NAItem: testName)
        if(singleTest){
             NotificationCenter.default.removeObserver(UIScreen.brightnessDidChangeNotification)
            dismiss(animated: true, completion: nil)
            
        }
        else{
             NotificationCenter.default.removeObserver(UIScreen.brightnessDidChangeNotification)
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: "Ambient")
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
             NotificationCenter.default.removeObserver(UIScreen.brightnessDidChangeNotification)
            dismiss(animated: true, completion: nil)
        }
        else{
             NotificationCenter.default.removeObserver(UIScreen.brightnessDidChangeNotification)
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
