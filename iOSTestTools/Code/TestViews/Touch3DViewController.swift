import UIKit
import Foundation
import MediaPlayer
import SwiftEntryKit

class Touch3DViewController: UIViewController {
    
    var testName = "3DTouch"
    
    @IBOutlet var Label3D: UILabel!
    @IBOutlet var Label10: UILabel!
    @IBOutlet var Label20: UILabel!
    @IBOutlet var Label40: UILabel!
    @IBOutlet var Label60: UILabel!
    @IBOutlet var Label80: UILabel!
    @IBOutlet var Label100: UILabel!
    var bool10 = false
    var bool20 = false
    var bool40 = false
    var bool60 = false
    var bool80 = false
    var bool100 = false
    var seguePath = "SegueToLCD"
     let mediumImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //checkTestList()
        
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
       
    }
    override func viewDidAppear(_ animated: Bool) {
         checkTestList()
        
        mediumImpactFeedbackGenerator.prepare()
        let userDefaults = UserDefaults.standard
        let model = userDefaults.string(forKey: "model")
        
//        if (model == "iPhone 6s" || model == "iPhone 6s Plus" || model == "iPhone 7" || model == "iPhone 7 Plus" || model == "iPhone 8" || model == "iPhone 8 Plus" || model == "iPhone X") {
//
//        }
//        else{
//            userDefaults.set("N/A", forKey: "3DTouch")
//            performSegue(withIdentifier: "SegueToLCD", sender: self)
//        }
        
        
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            var force = touch.force/touch.maximumPossibleForce*100
            force.round()
            Label3D.text = String(format: "%0.0f%%", force)
            if(bool10 && bool20 && bool40 && bool60 && bool80 && bool100){
                let userDefaults = UserDefaults.standard
                let singleTest = userDefaults.bool(forKey: "SingleTest")
                userDefaults.set("Pass", forKey: testName)
                //print("Test here")
                if(singleTest){
                    dismiss(animated: true, completion: nil)
                }
                else{
                    performSegue(withIdentifier: seguePath, sender: self)
                }
            }
            switch force {
            case 10:
                if(bool10 == false){
                    mediumImpactFeedbackGenerator.impactOccurred()
                }
                bool10 = true
                Label10.textColor = .green
                
            case 20:
                if(bool20 == false){
                    mediumImpactFeedbackGenerator.impactOccurred()
                }
                bool20 = true
                Label20.textColor = .green
               
            case 40:
                if(bool40 == false){
                    mediumImpactFeedbackGenerator.impactOccurred()
                }
                bool40 = true
                Label40.textColor = .green
               
            case 60:
                if(bool60 == false){
                    mediumImpactFeedbackGenerator.impactOccurred()
                }
                bool60 = true
                Label60.textColor = .green
               
            case 80:
                if(bool80 == false){
                    mediumImpactFeedbackGenerator.impactOccurred()
                }
                bool80 = true
                Label80.textColor = .green
                
            case 100:
                if(bool100 == false){
                    mediumImpactFeedbackGenerator.impactOccurred()
                }
                bool100 = true
                Label100.textColor = .green
                
                
            default:
                return
            }
        }
        
    }
        
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        Label3D.text = "0%"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func segue(){
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("Pass", forKey: testName)
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        SwiftEntryKit.dismiss()
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    if SwiftEntryKit.isCurrentlyDisplaying {
                        return
                    }
                    var attributes: EKAttributes
                    var imageName = "SIMFailed.png"
                    attributes = .bottomFloat
                    attributes.hapticFeedbackType = .success
                    attributes.screenInteraction = .dismiss
                    attributes.entryInteraction = .absorbTouches
                    attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
                    attributes.screenBackground = .color(color: .standardContent)
                    attributes.entryBackground = .color(color: .white)
                    attributes.entranceAnimation = .init(translate: .init(duration: 0.7, spring: .init(damping: 1, initialVelocity: 0)), scale: .init(from: 0.6, to: 1, duration: 0.7), fade: .init(from: 0.8, to: 1, duration: 0.3))
                     attributes.exitAnimation = .init(scale: .init(from: 1, to: 0.7, duration: 0.3), fade: .init(from: 1, to: 0, duration: 0.3))
                    attributes.displayDuration = .infinity
                    attributes.border = .value(color: .black, width: 0.5)
                    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 5))
                    attributes.statusBar = .dark
                    attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
                    //descriptionString = "Top floating alert view with button bar. Smooths in animately."
                    
                    let title = EKProperty.LabelContent(text: "Mark test manually", style: .init(font: MainFont.medium.with(size: 15), color: .black))
                    let description = EKProperty.LabelContent(text: "If you can't complete the test, you can mark it manually.", style: .init(font: MainFont.light.with(size: 13), color: .black))
                    let image = EKProperty.ImageContent(imageName: imageName, size: CGSize(width: 35, height: 35), contentMode: .scaleAspectFit)
                    let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
                    
                    // Generate buttons content
                    let buttonFont = MainFont.medium.with(size: 16)
                    
                    // Close button - Just dismiss entry when the button is tapped
                    let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let closeButtonLabel = EKProperty.LabelContent(text: "Not Available", style: closeButtonLabelStyle)
                    let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) {
                        self.markNA()
                        SwiftEntryKit.dismiss()
                    }
                    
                    
                    // Ok Button - Make transition to a new entry when the button is tapped
                    let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let okButtonLabel = EKProperty.LabelContent(text: "Failed", style: okButtonLabelStyle)
                    let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) { [unowned self] in
                        //var attributes = self.dataSource.bottomAlertAttributes
                        self.markFail()
                        SwiftEntryKit.dismiss()
                        
                    }
                    let buttonsBarContent = EKProperty.ButtonBarContent(with: closeButton, okButton, separatorColor: .standardContent, buttonHeight: 60, expandAnimatedly: false)
                    
                    // Generate the content
                    let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, imagePosition: .left, buttonBarContent: buttonsBarContent)
                    
                    let contentView = EKAlertMessageView(with: alertMessage)
                    
                    SwiftEntryKit.display(entry: contentView, using: attributes)
                }
            }
        }
    }
    func markNA(){
        sendNA(NAItem: "3DTouch")
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: testName)
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        performSegue(withIdentifier: seguePath, sender: self)
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
}
