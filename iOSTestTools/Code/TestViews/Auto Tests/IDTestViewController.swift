//
//  IDtestViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/9/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit
import LocalAuthentication
import CoreBluetooth
import CoreMotion
import CoreTelephony
import MediaPlayer

//import Reachability

class IDTestViewController: UIViewController, CBCentralManagerDelegate{
   
    
    @IBOutlet var NetworkLabel: UIImageView!
    
    @IBOutlet var detailLabel: UILabel!
    @IBOutlet var WifiLabel: UIImageView!
    //Setting the default sequential path
    var seguePath = "SegueToNFC"
    var reachability: Reachability?
    let hostNames = [nil, "google.com", "invalidhost"]
    var hostIndex = 0
    
    
    var cellChecked:Bool = false
    var wifiChecked:Bool = false
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            print("Bluetooth is on.")
            let userDefaults = UserDefaults.standard
            userDefaults.set("Pass", forKey: "Bluetooth")
            break
        case .poweredOff:
            print("Bluetooth is Off.")
            let userDefaults = UserDefaults.standard
            userDefaults.set("Fail", forKey: "Bluetooth")
            break
        case .resetting:
            print("Bluetooth is resetting.")
            break
        case .unauthorized:
            print("Bluetooth is unauthorized.")
            let userDefaults = UserDefaults.standard
            userDefaults.set("Fail", forKey: "Bluetooth")
            break
        case .unsupported:
            print("Bluetooth is unsupported.")
            let userDefaults = UserDefaults.standard
            userDefaults.set("Fail", forKey: "Bluetooth")
            break
        case .unknown:
            print("Bluetooth is unkown.")
            let userDefaults = UserDefaults.standard
            userDefaults.set("Fail", forKey: "Bluetooth")
            break
        default:
            break
        }
    }
    
    
    
    @IBOutlet var IDLabel: UILabel!
    var manager:CBCentralManager!
    
    
    let motion = CMMotionManager()
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
        
        
        manager = CBCentralManager()
        manager.delegate = self
        accelCheck()
        barometerCheck()
        SIMcheck()
        networkCheck()
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        //performSegue(withIdentifier: "SegueToNFC", sender: self)
        print("Test1")
        
        let userDefaults = UserDefaults.standard
        let model = userDefaults.string(forKey: "model")
        
        if (model == "iPhone 5s" || model == "iPhone SE" || model == "iPhone 6" || model == "iPhone 6 Plus" || model == "iPhone 6s" || model == "iPhone 6s Plus" || model == "iPhone 7" || model == "iPhone 7 Plus" || model == "iPhone 8" || model == "iPhone 8 Plus" || model == "iPhone X" || model == "iPad Air 2" || model == "iPad 5" || model == "iPad 6" || model == "iPad Pro 12.9") {
            print("Test2")
        }
        else{
            userDefaults.set("N/A", forKey: "IDAuth")
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
        var authError: NSError?
        if LAContext().canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            print("Test3")
            let userDefaults = UserDefaults.standard
            userDefaults.set("Pass", forKey: "IDAuth")
            // do your thing dependent on touch id being useable on the device
        }
        else{
            print("Test4")
            let userDefaults = UserDefaults.standard
            userDefaults.set("Fail", forKey: "IDAuth")
            sendFail(FailItem: "IDAuth")
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
    func networkCheck(){
        startHost(at: 0)
        
    }
    func bluetoothCheck(){
        
    }
    
    func SIMcheck(){
        let mobileNetworkCode = CTTelephonyNetworkInfo().subscriberCellularProvider?.mobileNetworkCode
        print(mobileNetworkCode ?? "MobileNetworkCode: N/A")
        if(mobileNetworkCode != nil){
            let userDefaults = UserDefaults.standard
            userDefaults.set("Pass", forKey: "SIM")
        }
        else{
            let userDefaults = UserDefaults.standard
            userDefaults.set("Fail", forKey: "SIM")
        }
        
    }
    func accelCheck(){
        if self.motion.isAccelerometerAvailable {
            let userDefaults = UserDefaults.standard
            userDefaults.set("Pass", forKey: "Accelerometer")
            print("Accelerometer is Good")
        }
        else{
            let userDefaults = UserDefaults.standard
            userDefaults.set("Fail", forKey: "Accelerometer")
            print("Accelerometer is Bad")
        }
    }
    
    func batteryCheck(){
        //something about battery health. Need to pull mAh values for devices and compare them to device values
        
        
    }
    func IDCheck(){
        
    }
    func barometerCheck(){
        //let altimeter = CMAltimeter()
        if CMAltimeter.isRelativeAltitudeAvailable() {
            let userDefaults = UserDefaults.standard
            userDefaults.set("Pass", forKey: "Barometer")
            print("Barometer Passed")
        }
        else{
            let userDefaults = UserDefaults.standard
            userDefaults.set("Fail", forKey: "Barometer")
        }
    }
    
    //    This is for Reachability and network detection
    
    
    func startHost(at index: Int) {
        stopNotifier()
        setupReachability(hostNames[index], useClosures: true)
        startNotifier()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.startHost(at: (index + 1) % 3)
        }
    }
    
    func setupReachability(_ hostName: String?, useClosures: Bool) {
        let reachability: Reachability?
        if let hostName = hostName {
            reachability = Reachability(hostname: hostName)
            // hostNameLabel.text = hostName
        } else {
            reachability = Reachability()
            //hostNameLabel.text = "No host name"
        }
        self.reachability = reachability
        // print("--- set up with host name: \(hostNameLabel.text!)")
        
        if useClosures {
            reachability?.whenReachable = { reachability in
                self.updateLabelColourWhenReachable(reachability)
            }
            reachability?.whenUnreachable = { reachability in
                self.updateLabelColourWhenNotReachable(reachability)
            }
        } else {
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(reachabilityChanged(_:)),
                name: .reachabilityChanged,
                object: reachability
            )
        }
    }
    
    func startNotifier() {
        print("--- start notifier")
        do {
            try reachability?.startNotifier()
        } catch {
            // networkStatus.textColor = .red
            // networkStatus.text = "Unable to start\nnotifier"
            return
        }
    }
    
    func stopNotifier() {
        print("--- stop notifier")
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
        reachability = nil
    }
    
    func updateLabelColourWhenReachable(_ reachability: Reachability) {
        print("\(reachability.description) - \(reachability.connection)")
        if reachability.connection == .wifi {
            //self.networkStatus.textColor = .green
             let userDefaults = UserDefaults.standard
             userDefaults.set("Pass", forKey: "Wifi")
            print("WIFI")
            wifiChecked = true
            WifiLabel.image = UIImage(named: "WifiEnabled.png")
            if(wifiChecked && cellChecked){
                
                let singleTest = userDefaults.bool(forKey: "SingleTest")
                
                if(singleTest){
                    dismiss(animated: true, completion: nil)
                }
                else{
                    performSegue(withIdentifier: seguePath, sender: self)
                }
            }
            else{
                detailLabel.text = "Turn Wifi Off, and make sure the device is connected through Cellular, otherwise hit N/A"
            }
        } else {
            //self.networkStatus.textColor = .blue
            print("cell")
            cellChecked = true
            let userDefaults = UserDefaults.standard
            userDefaults.set("Pass", forKey: "Cellular")
            NetworkLabel.image = UIImage(named: "NetworkEnabled.png")
            if(wifiChecked && cellChecked){
                stopNotifier()

                let userDefaults = UserDefaults.standard
                let singleTest = userDefaults.bool(forKey: "SingleTest")
                
                if(singleTest){
                    dismiss(animated: true, completion: nil)
                }
                else{
                    performSegue(withIdentifier: seguePath, sender: self)
                }
            }
            else{
                detailLabel.text = "Turn Wifi On and connect to a local Network, then return to this screen."
            }
        }
        
        //self.networkStatus.text = "\(reachability.connection)"
    }
    
    func updateLabelColourWhenNotReachable(_ reachability: Reachability) {
        //print("\(reachability.description) - \(reachability.connection)")
        
        // self.networkStatus.textColor = .red
        
        //self.networkStatus.text = "\(reachability.connection)"
    }
    
    @objc func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        
        if reachability.connection != .none {
            updateLabelColourWhenReachable(reachability)
        } else {
            updateLabelColourWhenNotReachable(reachability)
        }
    }
    
    deinit {
        stopNotifier()
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    print("Volume Button Pressed")
                    let alert = UIAlertController(title: "Select test status for Network", message: "", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "N/A", style: .default, handler: { action in
                        self.markNA()
                    } ))
                    alert.addAction(UIAlertAction(title: "Fail", style: .default, handler: { action in
                        self.markFail()
                    } ))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
                    self.present(alert, animated: true)
                }
            }
        }
    }
    func markNA(){
        stopNotifier()
        
        let userDefaults = UserDefaults.standard
        if(!cellChecked){
            sendNA(NAItem: "Cellular")
            userDefaults.set("N/A", forKey: "Cellular")
        }
        if(!wifiChecked){
            sendNA(NAItem: "Wifi")
            userDefaults.set("N/A", forKey: "Wifi")
        }
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markFail(){
        stopNotifier()
        let userDefaults = UserDefaults.standard
        if(!cellChecked){
            userDefaults.set("Fail", forKey: "Cell")
        }
        if(!wifiChecked){
            userDefaults.set("Fail", forKey: "Wifi")
        }
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
}

