//
//  ResultsViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/11/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit

class ResultsViewController: UIViewController{
    
    @IBOutlet var LCDLabel: UILabel!
    @IBOutlet var MultiTouchLabel: UILabel!
    @IBOutlet var TouchGridLabel: UILabel!
    @IBOutlet var ChargingLabel: UILabel!
    @IBOutlet var FrontCameraLabel: UILabel!
    @IBOutlet var RearCameraLabel: UILabel!
    @IBOutlet var VibrateLabel: UILabel!
    @IBOutlet var GyroLabel: UILabel!
    @IBOutlet var AudioLoopbackLabel: UILabel!
    @IBOutlet var IDLabel: UILabel!
    @IBOutlet var SpeakerLabel: UILabel!
    @IBOutlet var WOLabel: UILabel!
    @IBOutlet var IMEILabel: UILabel!
    @IBOutlet var ProxLabel: UILabel!

    @IBOutlet var AccelLabel: UILabel!
    @IBOutlet var BluetoothLabel: UILabel!
    @IBOutlet var BatteryLabel: UILabel!
    @IBOutlet var Touch3DLabel: UILabel!
    @IBOutlet var NFCLabel: UILabel!
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let userDefaults = UserDefaults.standard
        //sendLog(controller: self)
        
        let IMEI = userDefaults.string(forKey: "IMEI")
        
        let WO = userDefaults.string(forKey: "WO")
        let LCD = userDefaults.string(forKey: "LCD")
        let Touch3D = userDefaults.string(forKey: "3DTouch")
        let MultiTouch = userDefaults.string(forKey: "MultiTouch")
        
        let Touch = userDefaults.string(forKey: "TouchGrid")
        let Charging = userDefaults.string(forKey: "charging")
        let RearCamera = userDefaults.string(forKey: "RearCamera")
        let FrontCamera = userDefaults.string(forKey: "FrontCamera")
        let Gyro = userDefaults.string(forKey: "Gyroscope")
        let Speaker = userDefaults.string(forKey: "Speakers")
        let IDAuth = userDefaults.string(forKey: "IDAuth")
        let Loopback = userDefaults.string(forKey: "Loopback")
        let Vibration = userDefaults.string(forKey: "vibration")
        let Prox = userDefaults.string(forKey: "Prox")
        let Accel = userDefaults.string(forKey: "Accelerometer")
        let Bluetooth = userDefaults.string(forKey: "bluetooth")
        let Battery = userDefaults.string(forKey: "Battery")
        let NFC = userDefaults.string(forKey: "NFC")
        let Headphone = userDefaults.string(forKey: "Headphone")
        //let image = userDefaults.image(forKey: "Front Camera Image")
        
        
        
        
        
        
        WOLabel.text = WO
        IMEILabel.text = IMEI
        if(LCD == "Pass"){
            LCDLabel.text = "Pass"
            LCDLabel.textColor = .green
        }
        else if(LCD == "Fail"){
            LCDLabel.text = "Fail"
            LCDLabel.textColor = .red
        }
        else{
            LCDLabel.text = "N/A"
            LCDLabel.textColor = .yellow
        }
        if(MultiTouch == "Pass"){
            MultiTouchLabel.text = "Pass"
            MultiTouchLabel.textColor = .green
        }
        else if(MultiTouch == "Fail"){
            MultiTouchLabel.text = "Fail"
            MultiTouchLabel.textColor = .red
        }
        else{
            MultiTouchLabel.text = "N/A"
            MultiTouchLabel.textColor = .yellow
        }
        if(Accel == "Pass"){
            AccelLabel.text = "Pass"
            AccelLabel.textColor = .green
        }
        else if(Accel == "Fail"){
            AccelLabel.text = "Fail"
            AccelLabel.textColor = .red
        }
        else{
            AccelLabel.text = "N/A"
            AccelLabel.textColor = .yellow
        }
        if(Bluetooth == "Pass"){
            BluetoothLabel.text = "Pass"
            BluetoothLabel.textColor = .green
        }
        else if(Bluetooth == "Fail"){
            BluetoothLabel.text = "Fail"
            BluetoothLabel.textColor = .red
        }
        else{
            BluetoothLabel.text = "N/A"
            BluetoothLabel.textColor = .yellow
        }
        if(Touch3D == "Pass"){
            Touch3DLabel.text = "Pass"
            Touch3DLabel.textColor = .green
        }
        else if(Touch3D == "Fail"){
            Touch3DLabel.text = "Fail"
            Touch3DLabel.textColor = .red
        }
        else{
            Touch3DLabel.text = "N/A"
            Touch3DLabel.textColor = .yellow
        }
        if(Battery == "Pass"){
            BatteryLabel.text = "Pass"
            BatteryLabel.textColor = .green
        }
        else if(Battery == "Fail"){
            BatteryLabel.text = "Fail"
            BatteryLabel.textColor = .red
        }
        else{
            BatteryLabel.text = "N/A"
            BatteryLabel.textColor = .yellow
        }
        if(NFC == "Pass"){
            NFCLabel.text = "Pass"
            NFCLabel.textColor = .green
        }
        else if(NFC == "Fail"){
            NFCLabel.text = "Fail"
            NFCLabel.textColor = .red
        }
        else{
            NFCLabel.text = "N/A"
            NFCLabel.textColor = .yellow
        }
        if(Touch == "Pass"){
            TouchGridLabel.text = "Pass"
            TouchGridLabel.textColor = .green
        }
        else if(Touch == "Fail"){
            TouchGridLabel.text = "Fail"
            TouchGridLabel.textColor = .red
        }
        else{
            
            TouchGridLabel.text = "N/A"
            TouchGridLabel.textColor = .yellow
        }
        if(Charging == "Pass"){
            ChargingLabel.text = "Pass"
            ChargingLabel.textColor = .green
        }
        else if(Charging == "Fail"){
            ChargingLabel.text = "Fail"
            ChargingLabel.textColor = .red
        }
        else{
            ChargingLabel.text = "N/A"
            ChargingLabel.textColor = .yellow
        }
        if(RearCamera == "Pass"){
            RearCameraLabel.text = "Pass"
            RearCameraLabel.textColor = .green
        }
        else if(RearCamera == "Fail"){
            RearCameraLabel.text = "Fail"
            RearCameraLabel.textColor = .red
        }
        else{
            
            RearCameraLabel.text = "N/A"
            RearCameraLabel.textColor = .yellow
        }
        if(FrontCamera == "Pass"){
            FrontCameraLabel.text = "Pass"
            FrontCameraLabel.textColor = .green
        }
        else if(FrontCamera == "Fail"){
            FrontCameraLabel.text = "Fail"
            FrontCameraLabel.textColor = .red
        }
        else{
            FrontCameraLabel.text = "N/A"
            FrontCameraLabel.textColor = .yellow
            
        }
        if(Gyro == "Pass"){
            GyroLabel.text = "Pass"
            GyroLabel.textColor = .green
        }
        else if(Gyro == "Fail"){
            GyroLabel.text = "Fail"
            GyroLabel.textColor = .red
        }
        else{
            
            GyroLabel.text = "N/A"
            GyroLabel.textColor = .yellow
        }
        if(Speaker == "Pass"){
            SpeakerLabel.text = "Pass"
            SpeakerLabel.textColor = .green
        }
        else if(Speaker == "Fail"){
            SpeakerLabel.text = "Fail"
            SpeakerLabel.textColor = .red
        }
        else{
            
            SpeakerLabel.text = "N/A"
            SpeakerLabel.textColor = .yellow
        }
        if(IDAuth == "Pass"){
            IDLabel.text = "Pass"
            IDLabel.textColor = .green
        }
        else if(IDAuth == "Fail"){
            IDLabel.text = "Fail"
            IDLabel.textColor = .red
        }
        else{
            
            IDLabel.text = "N/A"
            IDLabel.textColor = .yellow
        }
        if(Loopback == "Pass"){
            AudioLoopbackLabel.text = "Pass"
            AudioLoopbackLabel.textColor = .green
        }
        else if(Loopback == "Fail"){
            AudioLoopbackLabel.text = "Fail"
            AudioLoopbackLabel.textColor = .red
        }
        else{
            AudioLoopbackLabel.text = "N/A"
            AudioLoopbackLabel.textColor = .yellow
        }
        if(Headphone == "Pass"){
            AudioLoopbackLabel.text = "Pass"
            AudioLoopbackLabel.textColor = .green
        }
        else if(Headphone == "Fail"){
            AudioLoopbackLabel.text = "Fail"
            AudioLoopbackLabel.textColor = .red
        }
        else{
            AudioLoopbackLabel.text = "N/A"
            AudioLoopbackLabel.textColor = .yellow
        }
        if(Vibration == "Pass"){
            VibrateLabel.text = "Pass"
            VibrateLabel.textColor = .green
        }
        else if(Vibration == "Fail"){
            VibrateLabel.text = "Fail"
            VibrateLabel.textColor = .red
        }
        else{
            VibrateLabel.text = "N/A"
            VibrateLabel.textColor = .yellow
        }
        if(Prox == "Pass"){
            ProxLabel.text = "Pass"
            ProxLabel.textColor = .green
        }
        else if(Prox == "Fail"){
            
            ProxLabel.text = "Fail"
            ProxLabel.textColor = .red
        }
        else{
            ProxLabel.text = "N/A"
            ProxLabel.textColor = .yellow
        }
    }
    func getSavedImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
    
    
    
}
