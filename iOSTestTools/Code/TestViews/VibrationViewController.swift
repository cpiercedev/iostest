//
//  VibrationViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/8/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox
import MediaPlayer
import SwiftEntryKit

class VibrationViewController: UIViewController{
    
    var testName = "Vibration"
    
    
    
    @IBOutlet var oneButton: UIButton!
    @IBOutlet var twoButton: UIButton!
    @IBOutlet var threeButton: UIButton!
    @IBOutlet var startButton: UIButton!
    var timer: Timer!
    var vibrationCount = 0
    var number = 0
    var failCount = 0
    var startPressed = false
    var seguePath = "SegueToBluetooth"
    var mediumImpactFeedbackGenerator: UIImpactFeedbackGenerator?
    
    @IBOutlet var PassFailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mediumImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
        startButton.isEnabled = true
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
        SwiftEntryKit.dismiss()
        mediumImpactFeedbackGenerator!.prepare()

        let userDefaults = UserDefaults.standard
        let model = userDefaults.string(forKey: "model")
        print(model)
        
        if (model == "iPhone 4" ||
            model == "iPhone 4s" ||
            model == "iPhone 5" ||
            model == "iPhone 5s" ||
            model == "iPhone 6"  ||
            model == "iPhone SE" ||
            model == "iPhone 6 Plus"  ||
            model == "iPhone 6s" ||
            model == "iPhone 6s Plus" ||
            model == "iPhone 7" ||
            model == "iPhone 7 Plus" ||
            model == "iPhone 8" ||
            model == "iPhone 8 Plus" ||
            model == "iPhone X")
        {
            print("it's in")
        }

        else{
            userDefaults.set("N/A", forKey: testName)
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
        
        oneButton.isEnabled = false
        twoButton.isEnabled = false
        threeButton.isEnabled = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func VibrationStartPressed(_ sender: Any) {
        mediumImpactFeedbackGenerator!.prepare()

        startPressed = true
        startButton.isEnabled = false
        oneButton.isEnabled = true
        twoButton.isEnabled = true
        threeButton.isEnabled = true
        number = Int.random(in: 1..<4)
        print("Button Pressed")
        var i = 0
        print(number)
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.loop), userInfo: nil, repeats: true)

//        while i < (3 * 1000) {
//            if(i%1000 == 0){
//                vibrate()
//                print(i)
//            }
//            i = i + 1
//
//
//        }

    }
        
        
        
    @objc
    func loop() {
        
        if (vibrationCount == number) {
            // your code here
            timer.invalidate()
            vibrationCount = 0
            return
        }
      
        vibrate()
        vibrationCount += 1
    }
        
        
        
    @IBAction func onePressed(_ sender: Any) {
        
        if(startPressed){
            
            oneButton.isEnabled = false
            twoButton.isEnabled = false
            threeButton.isEnabled = false
            if number == 1{
                PassFailLabel.text = "Pass"
                PassFailLabel.textColor = .green
                //sleep(1)
                let userDefaults = UserDefaults.standard
                print("true")
                userDefaults.set("Pass", forKey: testName)
                let singleTest = userDefaults.bool(forKey: "SingleTest")
                
                if(singleTest){
                    dismiss(animated: true, completion: nil)
                }
                else{
                    performSegue(withIdentifier: seguePath, sender: self)
                }
            }
            else{
                failCount = failCount + 1
                startButton.isEnabled = true
                PassFailLabel.text = "Fail"
                PassFailLabel.textColor = .red
            }
            if(failCount == 2){
                PassFailLabel.text = "Fail"
                PassFailLabel.textColor = .red
                //sleep(1)
               markFail()
            }
                startPressed = false
        }
    }
    @IBAction func twoPressed(_ sender: Any) {
        if(startPressed){
            oneButton.isEnabled = false
            twoButton.isEnabled = false
            threeButton.isEnabled = false
            if number == 2{
                PassFailLabel.text = "Pass"
                PassFailLabel.textColor = .green
                //sleep(1)
                let userDefaults = UserDefaults.standard
                print("true")
                userDefaults.set("Pass", forKey: testName)
                performSegue(withIdentifier: seguePath, sender: self)
            }
            else{
                PassFailLabel.text = "Fail"
                PassFailLabel.textColor = .red
                failCount = failCount + 1
                startButton.isEnabled = true
            }
            if(failCount == 2){
                PassFailLabel.text = "Fail"
                PassFailLabel.textColor = .red
                //sleep(1)
                let userDefaults = UserDefaults.standard
                markFail()
            }
                startPressed = false
        }
    }
    @IBAction func threePressed(_ sender: Any) {
       if(startPressed){
        oneButton.isEnabled = false
        twoButton.isEnabled = false
        threeButton.isEnabled = false
            if number == 3{
                PassFailLabel.text = "Pass"
                PassFailLabel.textColor = .green
                //sleep(1)
                
                let userDefaults = UserDefaults.standard
                userDefaults.set("Pass", forKey: testName)
                let singleTest = userDefaults.bool(forKey: "SingleTest")
                
                if(singleTest){
                    dismiss(animated: true, completion: nil)
                }
                else{
                    performSegue(withIdentifier: seguePath, sender: self)
                }
            }
            else{
                PassFailLabel.text = "Fail"
                PassFailLabel.textColor = .red
                failCount = failCount + 1
                startButton.isEnabled = true
            }
            if(failCount == 2){
                PassFailLabel.text = "Fail"
                PassFailLabel.textColor = .red
                //sleep(1)
                let userDefaults = UserDefaults.standard
                print("false")
                markFail()
            }
            startPressed = false
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        SwiftEntryKit.dismiss()
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    if SwiftEntryKit.isCurrentlyDisplaying {
                        return
                    }
                    var attributes: EKAttributes
                    var imageName = "SIMFailed.png"
                    attributes = .bottomFloat
                    attributes.hapticFeedbackType = .success
                    attributes.screenInteraction = .dismiss
                    attributes.entryInteraction = .absorbTouches
                    attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
                    attributes.screenBackground = .color(color: .standardContent)
                    attributes.entryBackground = .color(color: .white)
                    attributes.entranceAnimation = .init(translate: .init(duration: 0.7, spring: .init(damping: 1, initialVelocity: 0)), scale: .init(from: 0.6, to: 1, duration: 0.7), fade: .init(from: 0.8, to: 1, duration: 0.3))
                     attributes.exitAnimation = .init(scale: .init(from: 1, to: 0.7, duration: 0.3), fade: .init(from: 1, to: 0, duration: 0.3))
                    attributes.displayDuration = .infinity
                    attributes.border = .value(color: .black, width: 0.5)
                    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 5))
                    attributes.statusBar = .dark
                    attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
                    //descriptionString = "Top floating alert view with button bar. Smooths in animately."
                    
                    let title = EKProperty.LabelContent(text: "Mark test manually", style: .init(font: MainFont.medium.with(size: 15), color: .black))
                    let description = EKProperty.LabelContent(text: "If you can't complete the test, you can mark it manually.", style: .init(font: MainFont.light.with(size: 13), color: .black))
                    let image = EKProperty.ImageContent(imageName: imageName, size: CGSize(width: 35, height: 35), contentMode: .scaleAspectFit)
                    let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
                    
                    // Generate buttons content
                    let buttonFont = MainFont.medium.with(size: 16)
                    
                    // Close button - Just dismiss entry when the button is tapped
                    let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let closeButtonLabel = EKProperty.LabelContent(text: "Not Available", style: closeButtonLabelStyle)
                    let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) {
                        self.markNA()
                        SwiftEntryKit.dismiss()
                    }
                    
                    
                    // Ok Button - Make transition to a new entry when the button is tapped
                    let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let okButtonLabel = EKProperty.LabelContent(text: "Failed", style: okButtonLabelStyle)
                    let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) { [unowned self] in
                        //var attributes = self.dataSource.bottomAlertAttributes
                        self.markFail()
                        SwiftEntryKit.dismiss()
                        
                    }
                    let buttonsBarContent = EKProperty.ButtonBarContent(with: closeButton, okButton, separatorColor: .standardContent, buttonHeight: 60, expandAnimatedly: false)
                    
                    // Generate the content
                    let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, imagePosition: .left, buttonBarContent: buttonsBarContent)
                    
                    let contentView = EKAlertMessageView(with: alertMessage)
                    
                    SwiftEntryKit.display(entry: contentView, using: attributes)
                }
            }
        }
    }
    func markNA(){
        sendNA(NAItem: testName)
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: testName)
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
    func vibrate(){
        mediumImpactFeedbackGenerator!.prepare()
        if let feedbackSupportLevel = UIDevice.current.value(forKey: "_feedbackSupportLevel") as? Int {
            switch feedbackSupportLevel {
            case 2:
                // 2nd Generation Taptic Engine w/ Haptic Feedback (iPhone 7/7+)
                mediumImpactFeedbackGenerator!.impactOccurred()
               
                mediumImpactFeedbackGenerator!.prepare()

            case 1:
                // 1st Generation Taptic Engine (iPhone 6S/6S+)
                let peek = SystemSoundID(1519)
                AudioServicesPlaySystemSound(peek)
            case 0:
                // No Taptic Engine
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                break
            default: break
            }
            
        }
    }
}



