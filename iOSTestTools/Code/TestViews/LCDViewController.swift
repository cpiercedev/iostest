//
//  LCDViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/6/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer //Only for hidding  Volume view
import SwiftEntryKit

class LCDViewController: UIViewController {
    var count = 0
    var seguePath = "SegueToLoopback"
    var testName = "LCD"
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var PassButton: UIButton!
    @IBOutlet var FailButton: UIButton!
    @IBOutlet var ProgressDots: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 0))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
         checkTestList()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        SwiftEntryKit.dismiss()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func Touched(_ sender: Any) {
        count = count + 1
        switch (count) {
        case 1:
            ProgressDots.currentPage = 1
            backgroundView.backgroundColor = .green
            break;
        case 2:
            ProgressDots.currentPage = 2
            backgroundView.backgroundColor = .blue
            break;
        case 3:
            ProgressDots.currentPage = 3
            backgroundView.backgroundColor = .black
            break;
        case 4:
            ProgressDots.currentPage = 4
            backgroundView.backgroundColor = .white
            break;
        case 5:
            ProgressDots.alpha = 0.0
            PassButton.alpha = 1;
            PassButton.isEnabled = true;
            FailButton.alpha = 1;
            FailButton.isEnabled = true;
            break;
            
        default:
            break;
        }
        
    }
    @IBAction func PassButtonPressed(_ sender: Any) {
        let userDefaults = UserDefaults.standard

        userDefaults.set("Pass", forKey: testName)
        
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    @IBAction func FailButtonPressed(_ sender: Any) {
        let userDefaults = UserDefaults.standard
        
        userDefaults.set("Fail", forKey: testName)
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
        
        
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    if SwiftEntryKit.isCurrentlyDisplaying {
                        return
                    }
                    var attributes: EKAttributes
                    var imageName = "SIMFailed.png"
                    attributes = .topNote
                    attributes.name = "Top Note"
                    attributes.hapticFeedbackType = .success
                    attributes.popBehavior = .animated(animation: .translation)
                    attributes.entryBackground = .color(color: .standardBackground)
                    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 2))
                    attributes.statusBar = .light
                    
                    let text = "Manual marking is not available for this test."
                    let style = EKProperty.LabelStyle(font: MainFont.light.with(size: 14), color: .white, alignment: .center)
                    let labelContent = EKProperty.LabelContent(text: text, style: style)
                    
                    let contentView = EKNoteMessageView(with: labelContent)
                    
                    SwiftEntryKit.display(entry: contentView, using: attributes)
                    
                }
            }
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
}
