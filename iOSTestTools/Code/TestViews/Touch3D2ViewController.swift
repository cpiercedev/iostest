//
//  Touch3D2ViewController.swift
//  iOSTestTools
//
//  Created by Conor Pierce on 2/2/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import UIKit
import Foundation
import MediaPlayer
import SwiftEntryKit
import Lottie

class Touch3D2ViewController: UIViewController {
    @IBOutlet var TouchCircle: UIView!
    @IBOutlet var circle300: UIView!
    
    @IBOutlet var circle200: UIView!
    
    @IBOutlet var circle250: UIView!
    @IBOutlet var circle150: UIView!
    @IBOutlet var PressAnimation: AnimationView!
    
    var testName = "3DTouch"
    var seguePath = "SegueToLCD"
    var mediumImpactFeedbackGenerator: UIImpactFeedbackGenerator?

    var check150 = false
    var check200 = false
    var check250 = false
    var check300 = false
    override func viewDidLoad() {
        
        super.viewDidLoad()
        checkTestList()
        PressAnimation.loopMode = .loop
        PressAnimation.play()
        
        mediumImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)

        var shadowRad: CGFloat = 3.0
        var circleWidth: CGFloat = 7.0
        var shadowOp: Float = 0.25

        TouchCircle.layer.cornerRadius = min(TouchCircle.frame.size.height, TouchCircle.frame.size.width) / 2.0
        TouchCircle.clipsToBounds = true
        TouchCircle.layer.borderWidth = 5.0
        TouchCircle.layer.borderColor = .init(red: 1, green: 1, blue: 1, alpha: 1)
        TouchCircle.layer.shadowRadius = shadowRad
        TouchCircle.layer.shadowOpacity = shadowOp
        
        
        circle200.layer.cornerRadius = min(circle200.frame.size.height, circle200.frame.size.width) / 2.0
        circle200.clipsToBounds = true
        circle200.layer.borderWidth = circleWidth
        circle200.layer.borderColor = UIColor.systemPink.cgColor
        circle200.layer.shadowRadius = shadowRad
        circle200.layer.shadowOpacity = shadowOp
        
        circle300.layer.cornerRadius = min(circle300.frame.size.height, circle300.frame.size.width) / 2.0
        circle300.clipsToBounds = true
        circle300.layer.borderWidth = circleWidth
        circle300.layer.borderColor = UIColor.systemPink.cgColor
        circle300.layer.shadowRadius = shadowRad
        circle300.layer.shadowOpacity = shadowOp
        
        circle250.layer.cornerRadius = min(circle250.frame.size.height, circle250.frame.size.width) / 2.0
        circle250.clipsToBounds = true
        circle250.layer.borderWidth = circleWidth
        circle250.layer.borderColor = UIColor.systemPink.cgColor
        circle250.layer.shadowRadius = shadowRad
        circle250.layer.shadowOpacity = shadowOp
        
        
        circle150.layer.cornerRadius = min(circle150.frame.size.height, circle150.frame.size.width) / 2.0
        circle150.clipsToBounds = true
        circle150.layer.borderWidth = circleWidth
        circle150.layer.borderColor = UIColor.systemPink.cgColor
        circle150.layer.shadowRadius = shadowRad
        circle150.layer.shadowOpacity = shadowOp
        
        // Do any additional setup after loading the view.
    }
    func segue(){
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("Pass", forKey: testName)
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        mediumImpactFeedbackGenerator!.prepare()
        performSegue(withIdentifier: seguePath, sender: self)

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        SwiftEntryKit.dismiss()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        PressAnimation.pause()
        PressAnimation.alpha = 0
    }
//    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//        for touch in touches{
//            var force = touch.force/touch.maximumPossibleForce
//            //force.round()
//            
//            var circleSize = 100 + 200*force
//           
//            var centerX = TouchCircle.center.x - circleSize/2
//            var centerY = TouchCircle.center.y - circleSize/2
//            TouchCircle.frame =  CGRect(x:centerX, y: centerY, width: circleSize, height:circleSize)
//            TouchCircle.layer.cornerRadius = min(TouchCircle.frame.size.height, TouchCircle.frame.size.width) / 2.0
//            TouchCircle.clipsToBounds = true
//             circleSize.round()
//            if(circleSize == 200){
//                if(!check200){
//                circle200.layer.borderColor = UIColor.systemTeal.cgColor
//                 check200 = true
//                mediumImpactFeedbackGenerator!.impactOccurred()
//                mediumImpactFeedbackGenerator!.prepare()
//                }
//            }
//            if(circleSize == 300){
//                if(!check300){
//                circle300.layer.borderColor = UIColor.systemTeal.cgColor
//                check300 = true
//                    mediumImpactFeedbackGenerator!.impactOccurred()
//                    mediumImpactFeedbackGenerator!.prepare()
//                }
//
//            }
//            if(circleSize == 150){
//                if(!check150){
//                circle150.layer.borderColor = UIColor.systemTeal.cgColor
//                check150 = true
//                mediumImpactFeedbackGenerator!.impactOccurred()
//                mediumImpactFeedbackGenerator!.prepare()
//                }
//
//            }
//            if(circleSize == 250){
//                if(!check250){
//                circle250.layer.borderColor = UIColor.systemTeal.cgColor
//                check250 = true
//                mediumImpactFeedbackGenerator!.impactOccurred()
//                mediumImpactFeedbackGenerator!.prepare()
//                }
//
//            }
//            if(check150 && check200 && check250 && check300){
//                markPass()
//            }
//        }
//    
//    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        PressAnimation.play()
        PressAnimation.alpha = 1
        
        var pointx = self.view.center.x - 50
        var pointy = self.view.center.y - 50
        
        TouchCircle.frame =  CGRect(x:pointx, y: pointy, width: 100, height:100)
        TouchCircle.layer.cornerRadius = min(TouchCircle.frame.size.height, TouchCircle.frame.size.width) / 2.0
        TouchCircle.clipsToBounds = true
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    if SwiftEntryKit.isCurrentlyDisplaying {
                        return
                    }
                    var attributes: EKAttributes
                    var imageName = "SIMFailed.png"
                    attributes = .bottomFloat
                    attributes.hapticFeedbackType = .success
                    attributes.screenInteraction = .dismiss
                    attributes.entryInteraction = .absorbTouches
                    attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
                    attributes.screenBackground = .color(color: .standardContent)
                    attributes.entryBackground = .color(color: .white)
                    attributes.entranceAnimation = .init(translate: .init(duration: 0.7, spring: .init(damping: 1, initialVelocity: 0)), scale: .init(from: 0.6, to: 1, duration: 0.7), fade: .init(from: 0.8, to: 1, duration: 0.3))
                    attributes.exitAnimation = .init(scale: .init(from: 1, to: 0.7, duration: 0.3), fade: .init(from: 1, to: 0, duration: 0.3))
                    attributes.displayDuration = .infinity
                    attributes.border = .value(color: .black, width: 0.5)
                    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 5))
                    attributes.statusBar = .dark
                    attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
                    //descriptionString = "Top floating alert view with button bar. Smooths in animately."
                    
                    let title = EKProperty.LabelContent(text: "Mark test manually", style: .init(font: MainFont.medium.with(size: 15), color: .black))
                    let description = EKProperty.LabelContent(text: "If you can't complete the test, you can mark it manually.", style: .init(font: MainFont.light.with(size: 13), color: .black))
                    let image = EKProperty.ImageContent(imageName: imageName, size: CGSize(width: 35, height: 35), contentMode: .scaleAspectFit)
                    let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
                    
                    // Generate buttons content
                    let buttonFont = MainFont.medium.with(size: 16)
                    
                    // Close button - Just dismiss entry when the button is tapped
                    let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let closeButtonLabel = EKProperty.LabelContent(text: "Not Available", style: closeButtonLabelStyle)
                    let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) {
                        self.markNA()
                        SwiftEntryKit.dismiss()
                    }
                    
                    
                    // Ok Button - Make transition to a new entry when the button is tapped
                    let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let okButtonLabel = EKProperty.LabelContent(text: "Failed", style: okButtonLabelStyle)
                    let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) { [unowned self] in
                        //var attributes = self.dataSource.bottomAlertAttributes
                        self.markFail()
                        SwiftEntryKit.dismiss()
                        
                    }
                    let buttonsBarContent = EKProperty.ButtonBarContent(with: closeButton, okButton, separatorColor: .standardContent, buttonHeight: 60, expandAnimatedly: false)
                    
                    // Generate the content
                    let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, imagePosition: .left, buttonBarContent: buttonsBarContent)
                    
                    let contentView = EKAlertMessageView(with: alertMessage)
                    
                    SwiftEntryKit.display(entry: contentView, using: attributes)
                }
            }
        }
    }
    func markPass(){
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("Pass", forKey: testName)
        //print("Test here")
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markNA(){
        sendNA(NAItem: "3DTouch")
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: testName)
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        performSegue(withIdentifier: seguePath, sender: self)
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
