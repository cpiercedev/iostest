//
//  Camera1ViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 10/20/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//


import UIKit
import AVFoundation
import MediaPlayer
import SwiftEntryKit


class Camera1ViewController: UIViewController {
    
    var testName = "RearCamera"
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    
    var captureSession: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var capturePhotoOutput: AVCapturePhotoOutput?
    var qrCodeFrameView: UIView?
    var seguePath = "SegueToFrontCamera"
    var firstTested = false
    var count = 0
    var cameraFound = true
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
    
        
    
        captureButton.layer.cornerRadius = captureButton.frame.size.width / 2
        captureButton.clipsToBounds = true
        
        // Get an instance of the AVCaptureDevice class to initialize a device object and provide the video as the media type parameter
        guard let captureDevice = AVCaptureDevice.default(.builtInWideAngleCamera,for: .video, position: .back) else {
            //fatalError("No video device found")
            cameraFound = false
            return
        }
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous deivce object
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Initialize the captureSession object
            captureSession = AVCaptureSession()
            
            // Set the input devcie on the capture session
            captureSession?.addInput(input)
            
            // Get an instance of ACCapturePhotoOutput class
            capturePhotoOutput = AVCapturePhotoOutput()
            capturePhotoOutput?.isHighResolutionCaptureEnabled = true
            
            // Set the output on the capture session
            captureSession?.addOutput(capturePhotoOutput!)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the input device
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self as AVCaptureMetadataOutputObjectsDelegate, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
            //Initialise the video preview layer and add it as a sublayer to the viewPreview view's layer
            videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
            videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            videoPreviewLayer?.frame = view.layer.bounds
            previewView.layer.addSublayer(videoPreviewLayer!)
            
            
            //start video capture
            captureSession?.startRunning()
            
            messageLabel.isHidden = true
            
            //Initialize QR Code Frame to highlight the QR code
            qrCodeFrameView = UIView()
            //            overView = UIView()
            //            if let overView = qrCodeFrameView {
            //                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            //                qrCodeFrameView.layer.borderWidth = 2
            //                view.addSubview(qrCodeFrameView)
            //                view.bringSubview(toFront: qrCodeFrameView)
            //            }
            
            if let qrCodeFrameView = qrCodeFrameView {
                qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
                qrCodeFrameView.layer.borderWidth = 2
                view.addSubview(qrCodeFrameView)
                view.bringSubviewToFront(qrCodeFrameView)
            }
        } catch {
            //If any error occurs, simply print it out
            print(error)
            return
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
        
        
        if(!cameraFound){
            let alert = UIAlertController(title: "No Camera Available", message: "The device could not locate a Rear Camera.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { action in
            }))
            
            self.present(alert, animated: true)
            messageLabel.text = "No Camera Found"
        }
        else{
            var attributes: EKAttributes
            var imageName = "SIMFailed.png"
            attributes = .topNote
            attributes.displayDuration = .infinity
            attributes.name = "Top Note"
            attributes.hapticFeedbackType = .success
            attributes.popBehavior = .animated(animation: .translation)
            attributes.entryBackground = .color(color: .standardContent)
            attributes.shadow = .active(with: .init(color: .standardContent, opacity: 0.5, radius: 2))
            attributes.statusBar = .light
            
            let text = "Hold a QR code in front of the camera to pass."
            let style = EKProperty.LabelStyle(font: MainFont.light.with(size: 14), color: .white, alignment: .center)
            let labelContent = EKProperty.LabelContent(text: text, style: style)
            
            let contentView = EKNoteMessageView(with: labelContent)
            
            SwiftEntryKit.display(entry: contentView, using: attributes)
        }
        
        
        //
        
    }
    override func viewDidLayoutSubviews() {
        videoPreviewLayer?.frame = view.bounds
        if let previewLayer = videoPreviewLayer ,(previewLayer.connection?.isVideoOrientationSupported)! {
            previewLayer.connection?.videoOrientation = UIApplication.shared.statusBarOrientation.videoOrientation ?? .portrait
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        //SwiftEntryKit.dismiss()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    if SwiftEntryKit.isCurrentlyDisplaying {
                        return
                    }
                    var attributes: EKAttributes
                    var imageName = "SIMFailed.png"
                    attributes = .bottomFloat
                    attributes.hapticFeedbackType = .success
                    attributes.screenInteraction = .dismiss
                    attributes.entryInteraction = .absorbTouches
                    attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
                    attributes.screenBackground = .color(color: .standardContent)
                    attributes.entryBackground = .color(color: .white)
                    attributes.entranceAnimation = .init(translate: .init(duration: 0.7, spring: .init(damping: 1, initialVelocity: 0)), scale: .init(from: 0.6, to: 1, duration: 0.7), fade: .init(from: 0.8, to: 1, duration: 0.3))
                     attributes.exitAnimation = .init(scale: .init(from: 1, to: 0.7, duration: 0.3), fade: .init(from: 1, to: 0, duration: 0.3))
                    attributes.displayDuration = .infinity
                    attributes.border = .value(color: .black, width: 0.5)
                    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 5))
                    attributes.statusBar = .dark
                    attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
                    //descriptionString = "Top floating alert view with button bar. Smooths in animately."
                    
                    let title = EKProperty.LabelContent(text: "Mark test manually", style: .init(font: MainFont.medium.with(size: 15), color: .black))
                    let description = EKProperty.LabelContent(text: "If you can't complete the test, you can mark it manually.", style: .init(font: MainFont.light.with(size: 13), color: .black))
                    let image = EKProperty.ImageContent(imageName: imageName, size: CGSize(width: 35, height: 35), contentMode: .scaleAspectFit)
                    let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
                    
                    // Generate buttons content
                    let buttonFont = MainFont.medium.with(size: 16)
                    
                    // Close button - Just dismiss entry when the button is tapped
                    let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let closeButtonLabel = EKProperty.LabelContent(text: "Not Available", style: closeButtonLabelStyle)
                    let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) {
                        self.markNA()
                        SwiftEntryKit.dismiss()
                    }
                    
                    
                    // Ok Button - Make transition to a new entry when the button is tapped
                    let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let okButtonLabel = EKProperty.LabelContent(text: "Failed", style: okButtonLabelStyle)
                    let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) { [unowned self] in
                        //var attributes = self.dataSource.bottomAlertAttributes
                        self.markFail()
                        SwiftEntryKit.dismiss()
                        
                    }
                    let buttonsBarContent = EKProperty.ButtonBarContent(with: closeButton, okButton, separatorColor: .standardContent, buttonHeight: 60, expandAnimatedly: false)
                    
                    // Generate the content
                    let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, imagePosition: .left, buttonBarContent: buttonsBarContent)
                    
                    let contentView = EKAlertMessageView(with: alertMessage)
                    
                    SwiftEntryKit.display(entry: contentView, using: attributes)
                }
            }
        }
    }
    func markNA(){
        sendNA(NAItem: "RearCamera")
        let userDefaults = UserDefaults.standard
        userDefaults.set("N/A", forKey: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func markFail(){
        
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            
            if(singleTest){
                dismiss(animated: true, completion: nil)
            }
            else{
                performSegue(withIdentifier: seguePath, sender: self)
            }
        }
    }
}

extension Camera1ViewController : AVCapturePhotoCaptureDelegate {
    func photoOutput(_ captureOutput: AVCapturePhotoOutput,
                     didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?,
                     previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?,
                     resolvedSettings: AVCaptureResolvedPhotoSettings,
                     bracketSettings: AVCaptureBracketedStillImageSettings?,
                     error: Error?) {
        // Make sure we get some photo sample buffer
        guard error == nil,
            let photoSampleBuffer = photoSampleBuffer else {
                print("Error capturing photo: \(String(describing: error))")
                return
        }
        
        // Convert photo same buffer to a jpeg image data by using AVCapturePhotoOutput
        guard let imageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer, previewPhotoSampleBuffer: previewPhotoSampleBuffer) else {
            return
        }
        
        // Initialise an UIImage with our image data
        let capturedImage = UIImage.init(data: imageData , scale: 1.0)
        if let image = capturedImage {
            // Save our captured image to photos album
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
    }
}

extension Camera1ViewController : AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ captureOutput: AVCaptureMetadataOutput,
                        didOutput metadataObjects: [AVMetadataObject],
                        from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            messageLabel.isHidden = true
            count = 0
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObject.ObjectType.qr {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                messageLabel.isHidden = false
                if(count > 40){
                let userDefaults = UserDefaults.standard
                
                    messageLabel.text = "Passed"
                    userDefaults.set("Pass", forKey: testName)
                
                    
                    count += 1
                    if(count >= 50){
               // performSegue(withIdentifier: "SegueToFrontCamera", sender: self)
                let singleTest = userDefaults.bool(forKey: "SingleTest")

                if(singleTest){
                    dismiss(animated: true, completion: nil)
                }
                else{
                    performSegue(withIdentifier: seguePath, sender: self)
                }
                    }
                }
                else{
                    count += 1
                    print(count)
                }
            }
        }
    }
}

extension UIInterfaceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeRight: return .landscapeRight
        case .landscapeLeft: return .landscapeLeft
        case .portrait: return .portrait
        default: return nil
        }
    }
}



