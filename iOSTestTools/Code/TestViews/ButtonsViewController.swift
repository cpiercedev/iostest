//
//  ButtonsViewController.swift
//  iOSTestTools
//
//  Created by Conor Pierce on 4/29/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import UIKit
import MediaPlayer
import Lottie

class ButtonsViewController: UIViewController {
    var currentVolume: Float?
    
    @IBOutlet var DescriptionText: UILabel!
    @IBOutlet var AnimationButtons: UIImageView!
    
    
    var currentTest = 0
    override func viewDidLoad() {
        DescriptionText.text = "Press the Volume Up Button"
        
//        let wifiGif = UIImage.gifImageWithName("Wifi")
//        AnimationButtons = UIImageView(image: wifiGif)
        super.viewDidLoad()
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        currentVolume = AVAudioSession.sharedInstance().outputVolume
        
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(forName: UIApplication.userDidTakeScreenshotNotification, object: nil, queue: OperationQueue.main) { notification in
            print("Screenshot taken!")
            if(self.currentTest == 2){
                self.DescriptionText.text = "Your Buttons are Working"
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        //ButtonAnimation.play()
    }
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    print("Current Volume \(currentVolume)")
                    let volume = notification.userInfo!["AVSystemController_AudioVolumeNotificationParameter"] as! Float
                    if( volume < currentVolume!){
                        
                        print("Volume down Pressed")
                        if(currentTest == 1){
                            DescriptionText.text = "Press Volume up and the Sleep/Wake Button to take a screenshot."
                            
                            currentTest = 2
                        }
                    }
                    else{
                        print("Volume up Pressed")
                        if(currentTest == 0){
                            DescriptionText.text = "Press the Volume Down Button"
                            currentTest = 1
//                            ButtonAnimation.animation = Animation.named("VolumeDownX")
//                            ButtonAnimation.play()
                        }
                        
                    }
                    currentVolume = volume
                    
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
