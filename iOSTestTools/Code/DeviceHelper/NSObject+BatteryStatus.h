//
//  NSObject+BatteryStatus.h
//  DeviceTesting
//
//  Created by Conor Pierce on 7/10/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSObject (BatteryStatus)

-(NSDictionary *) FCPrivateBatteryStatus;
@end

