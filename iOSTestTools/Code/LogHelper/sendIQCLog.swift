//
//  sendLog.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 7/7/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import Firebase
import UIKit

func sendIQCLog(controller: UIViewController){
    let userDefaults = UserDefaults.standard
    var testResults: [String] = []
    
    let IMEI = userDefaults.string(forKey: "IMEI")
    let Serial = userDefaults.string(forKey: "Serial")
    let WO = userDefaults.string(forKey: "WO")
    let SystemVersion = userDefaults.string(forKey: "iOSVersion")
    let StoreNumber = (userDefaults.string(forKey: "StoreNumber") ?? "N/A")
    let Model = userDefaults.string(forKey: "model")
    let StartTime = userDefaults.object(forKey: "StartTime")
    let EndTime = Date()
    let modelID = userDefaults.string(forKey: "modelID")
    
    let LCD = userDefaults.string(forKey: "LCD")
    testResults.append(LCD ?? "False")
    
    let Gyro = userDefaults.string(forKey: "Gyroscope")
    testResults.append(Gyro ?? "False")
    
    let IDAuth = userDefaults.string(forKey: "IDAuth")
    testResults.append(IDAuth ?? "False")
    
    
    let NFC = userDefaults.string(forKey: "NFC")
    testResults.append(NFC ?? "False")
    
    let Bluetooth = userDefaults.string(forKey: "Bluetooth")
    testResults.append(Bluetooth ?? "False")
    
    let Proximity = userDefaults.string(forKey: "Prox")
    testResults.append(Proximity ?? "False")
    
    let accel = userDefaults.string(forKey: "Accelerometer")
    testResults.append(accel ?? "False")
    
    let Barometer = userDefaults.string(forKey: "Barometer")
    testResults.append(Barometer ?? "False")
    
    let SIM = userDefaults.string(forKey: "SIM")
    testResults.append(SIM ?? "False")
    
    let Wifi = userDefaults.string(forKey: "Wifi")
    testResults.append(Wifi ?? "False")
    
    let Cell = userDefaults.string(forKey: "Cellular")
    testResults.append(Cell ?? "False")
    
    let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"]  as? String
    
    let DocID = userDefaults.string(forKey: "DocID")
    let db = Firestore.firestore()
    let settings = db.settings
    settings.areTimestampsInSnapshotsEnabled = true
    db.settings = settings
    
    
    print(DocID ?? "N/A")
    
    
    let documentID = DocID ?? "N/A"
    
    let  docRef = db.collection("users").document(documentID);
    
    var result = "Pass"
    for tests in testResults{
        if(tests == "Fail"){
            result = "Fail"
        }
    }
    updateTotalIQC(result: result)
    //logIDIQC(result: result)
    var tokenCount = 0
    
    docRef.getDocument { (document, error) in
        if let document = document, document.exists {
            
            print("\(document.documentID) => \(document.data())")
            print(document.data()!["Tokens"] as! Int)
            
            tokenCount = document.data()!["Tokens"] as! Int
            var uses = document.data()!["Uses"] as! Int
            var passCount = document.data()!["PassCount"] as! Int
            var failCount = document.data()!["FailCount"] as! Int
            
            if(result == "Pass"){
                passCount += 1
            }
            else{
                failCount += 1
            }
            
            print("User has \(tokenCount) tokens")
            tokenCount -= 1
            uses += 1
            print("\(tokenCount) Tokens's remaining")
            docRef.updateData([
                "Tokens": tokenCount,
                "Uses": uses,
            ]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Document successfully updated")
                }
            }
            
        } else {
            print("Document does not exist")
        }
    }
    
    
    
    var WOLink = "https://portal.ubif.net/pos/workorder/" + WO!
    
    var ref: DocumentReference? = nil
    
    ref = db.collection("IQCLogs").addDocument(data: [
        "StartTime": StartTime,
        "DateTime": Date(),
        "IMEI": IMEI ?? "N/A",
        "Serial": Serial ?? "N/A",
        "WO": WO!,
        "LCD": LCD,
        "StoreNumber": StoreNumber,
        "Gyroscope": Gyro,
        "Pass": result,
        "IDAuth": IDAuth,
        "Accelerometer": accel,
        "NFC": NFC,
        "Bluetooth": Bluetooth,
        "Proximity": Proximity,
        "Model": Model,
        "Barometer": Barometer,
        "SIM": SIM,
        "Wifi": Wifi,
        "Cellular": Cell,
        "WOLink": WOLink,
        "Version": version,
        "Identifier": modelID
        //"iOS Version": SystemVersion
        
        
        
    ]) { err in
        if let err = err {
            print("Error adding document: \(err)")
        } else {
            print("Document added with ID: \(ref!.documentID)")
            logIDIQC(result: result, IQCID: ref!.documentID)
            
            let alert = UIAlertController(title: "Log Saved", message: "Remember to delete the app.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { action in
            }))
            
            controller.present(alert, animated: true)
        }
    }
    
    
}

func updateTotalIQC(result: String){
    if(StoreNumber == "1"){
    //return
    }
    let db = Firestore.firestore()
    let settings = db.settings
    settings.areTimestampsInSnapshotsEnabled = true
    db.settings = settings
    
    let  docRef = db.collection("TotalUsage").document("Usage");
    
    docRef.getDocument { (document, error) in
        if let document = document, document.exists {
            var total = 0
            var pass = 0
            var fail = 0
            //var unlocks = document.data()!["TotalUnlocks"] as! Int
            if(document.data()!["TotalIQCFail"] != nil){
                 fail = document.data()!["TotalIQCFail"] as! Int
            }
            if(document.data()!["TotalIQCPass"] != nil){
                 pass = document.data()!["TotalIQCPass"] as! Int
            }
            if(document.data()!["TotalIQC"] != nil){
                total = document.data()!["TotalIQC"] as! Int
            }
            
            total += 1
            if(totalUpdated){
                //return
            }
            totalUpdated = true
            if(result == "Pass"){
                pass += 1
            }
            else{
                fail += 1
            }
            
            
            docRef.updateData([
                "TotalIQCFail": fail,
                "TotalIQCPass": pass,
                "TotalIQC": total,
                
            ]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Total Document successfully updated")
                }
            }
            
        } else {
            print("Document does not exist TU")
        }
    }
}
func logIDIQC(result: String, IQCID: String){
    let userDefaults = UserDefaults.standard
    var cell = userDefaults.bool(forKey: "CellularModel")
    var deviceID: String?
    if(cell){
        deviceID = userDefaults.string(forKey: "IMEI")
    }
    else{
        deviceID = userDefaults.string(forKey: "Serial")
    }
    let WO = userDefaults.string(forKey: "WO")
    let StartTime = userDefaults.object(forKey: "StartTime")
    var Model = userDefaults.string(forKey: "model")
    let db = Firestore.firestore()
    let settings = db.settings
    var willUpdate = false
    var forcedResult = result
    settings.areTimestampsInSnapshotsEnabled = true
    db.settings = settings
    
    if let uuid = UIDevice.current.identifierForVendor?.uuidString {
        print(uuid)
        db.collection("Devices").whereField("UUID", isEqualTo: uuid)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        //var currentState = document.data()[Nested] as! String
                        //                        if(currentState == "Pass"){
                        //                            forcedResult = "Pass"
                        //                        }
                        //print("\(document.documentID) => \(document.data())")
                    }
                }
        }
        let  docRef = db.collection("Devices/\(deviceID!)/Workorders").document(WO!);
        docRef.getDocument { (document, error) in
            if let document = document {
                if document.exists{
                    
                    docRef.updateData([
                        "IQC": true,
                        "IQCResult": result,
                        "IQCToken": false,
                        "IQCID": IQCID,
                        
                    ]) { err in
                        if let err = err {
                            print("Error updating document: \(err)")
                            
                        } else {
                            print("Document successfully updated")
                        }
                    }
                    
                } else {
                    print("Document does not exist")
                    
                    
                }
            }
        }
        
    }
}
