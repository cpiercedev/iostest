//
 //  RoundCorners.swift
 //  iOSTestTools
 //
 //  Created by Conor Pierce on 2/13/19.
 //  Copyright © 2019 Conor Pierce. All rights reserved.
 //
 
 import UIKit
 import SwiftEntryKit
 
 var topAttributes: EKAttributes {
     var attributes = EKAttributes.topFloat
     attributes.hapticFeedbackType = .success
     attributes.windowLevel = .normal
     attributes.displayDuration = .infinity
     attributes.entryBackground = .color(color: .white)
     attributes.screenBackground = .color(color: EKColor(UIColor(white: 0.5, alpha: 0.5)))
     attributes.shadow = .active(with: .init(color: .black, opacity: 0.3, radius: 8))
     attributes.screenInteraction = .absorbTouches
     attributes.entryInteraction = .absorbTouches
     attributes.scroll = .disabled
     attributes.roundCorners = .all(radius: 25)
     attributes.entranceAnimation = .init(translate: .init(duration: 0.7, spring: .init(damping: 1, initialVelocity: 0)),
                                          scale: .init(from: 1.05, to: 1, duration: 0.4, spring: .init(damping: 1, initialVelocity: 0)))
     attributes.exitAnimation = .init(translate: .init(duration: 0.2))
     attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.2)))
     attributes.positionConstraints.verticalOffset = 10
     attributes.positionConstraints.size = .init(width: .offset(value: 20), height: .intrinsic)
     attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
     attributes.statusBar = .dark
     return attributes
     
 }
 var botttomAttributes: EKAttributes {
     var attributes = EKAttributes.topFloat
     attributes.hapticFeedbackType = .success
     attributes.windowLevel = .normal
     attributes.displayDuration = .infinity
     attributes.entryBackground = .color(color: .white)
     attributes.screenBackground = .color(color: EKColor(UIColor(white: 0.5, alpha: 0.5)))
     attributes.shadow = .active(with: .init(color: .black, opacity: 0.3, radius: 8))
     attributes.screenInteraction = .absorbTouches
     attributes.entryInteraction = .absorbTouches
     attributes.scroll = .disabled
     attributes.roundCorners = .all(radius: 25)
     attributes.entranceAnimation = .init(translate: .init(duration: 0.7, spring: .init(damping: 1, initialVelocity: 0)),
                                          scale: .init(from: 1.05, to: 1, duration: 0.4, spring: .init(damping: 1, initialVelocity: 0)))
     attributes.exitAnimation = .init(translate: .init(duration: 0.2))
     attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.2)))
     attributes.positionConstraints.verticalOffset = 10
     attributes.positionConstraints.size = .init(width: .offset(value: 20), height: .intrinsic)
     attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
     attributes.statusBar = .dark
     return attributes
     
 }
 func roundCorners (viewBox: UIView){
     
     viewBox.layer.cornerRadius = 25.0
     viewBox.clipsToBounds = true
     viewBox.layer.shadowColor = UIColor.black.cgColor
     viewBox.layer.shadowRadius = 5.0
     viewBox.layer.shadowOpacity = 0.5
     viewBox.layer.shadowOffset = CGSize(width: 1, height: 1)
     viewBox.layer.masksToBounds = false
 }
