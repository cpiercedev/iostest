//
//  ManualSelect.swift
//  iOSTestTools
//
//  Created by Conor Pierce on 2/2/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import UIKit


class ManualSelect: UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    init() {
        super.init(frame: .zero)
        setup()
    }
    
    private func setup() {
        fromNib()
        clipsToBounds = true
        layer.cornerRadius = 5
    }
    

}
func segue(controller: UIViewController){
    controller.performSegue(withIdentifier: "Something", sender: controller)
    
}
