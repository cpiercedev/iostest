//
//  KeyViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 8/27/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import CoreTelephony
import MediaPlayer //Only for hidding  Volume view
//import Crashlytics
import CoreNFC
import SwiftEntryKit
//import QuickLayout

class KeyViewController: UIViewController, NFCNDEFReaderSessionDelegate {

    @IBOutlet var viewBox: UIView!
    @IBOutlet var TextBox: UITextField!
    @IBOutlet var buildLabel: UILabel!
    var totalUpdated = false
    var nfcSession: NFCNDEFReaderSession?
    var this = self
    var storeNumber: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roundCorners(viewBox: viewBox)
        
        setTestList()

        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 0))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
       
        
        // Do any additional setup after loading the view.
        if let text = Bundle.main.infoDictionary?["CFBundleShortVersionString"]  as? String {
            //print(text)
            buildLabel.text = "Version: \(text)"
        }

    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        let alert = UIAlertController(title: "NFC to login", message: "You can login with NFC by pressing a volume button on this screen.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { action in
        }))
        var attributes: EKAttributes
        
        // Preset I
        attributes = topAttributes
        
        //showDarkAwesomePopupMessage(attributes: attributes)
        //self.present(alert, animated: true)
        
        
        
    }
    private func showDarkAwesomePopupMessage(attributes: EKAttributes) {
            let image = UIImage(named: "nfc_3834.png")!
            let title = "NFC to login"
            let description = "You can login with NFC by pressing a volume button on this screen."
            showPopupMessage(attributes: attributes, title: title, titleColor: .darkText, description: description, descriptionColor: .darkSubText, buttonTitleColor: .white, buttonBackgroundColor: .satCyan, image: image)
        }
        private func showPopupMessage(attributes: EKAttributes, title: String, titleColor: UIColor, description: String, descriptionColor: UIColor, buttonTitleColor: UIColor, buttonBackgroundColor: UIColor, image: UIImage? = nil) {
            
            var themeImage: EKPopUpMessage.ThemeImage?
            
            if let image = image {
                themeImage = .init(image: .init(image: image, size: CGSize(width: 60, height: 60), contentMode: .scaleAspectFit))
            }
            
            let title = EKProperty.LabelContent(text: title, style: .init(font: MainFont.medium.with(size: 24), color: EKColor(titleColor), alignment: .center))
            let description = EKProperty.LabelContent(text: description, style: .init(font: MainFont.light.with(size: 16), color: EKColor(descriptionColor), alignment: .center))
            let button = EKProperty.ButtonContent(label: .init(text: "Okay", style: .init(font: MainFont.bold.with(size: 16), color: EKColor(buttonTitleColor))), backgroundColor: EKColor(buttonBackgroundColor), highlightedBackgroundColor: EKColor(buttonTitleColor.withAlphaComponent(0.05)))
            let message = EKPopUpMessage(themeImage: themeImage, title: title, description: description, button: button) {
                
                SwiftEntryKit.dismiss()
            }
            
            let contentView = EKPopUpMessageView(with: message)
            SwiftEntryKit.display(entry: contentView, using: attributes)
        }

    
    @IBAction func ButtonPressed(_ sender: Any) {
        //Crashlytics.sharedInstance().crash()
        if(TextBox.text == "CPIERCEToken"){
            self.performSegue(withIdentifier: "SegueToToken", sender: self)
        }
        //buttonAction()
        logIn(key: (TextBox.text ?? "n/a"))
    }
    func NFC(){
        nfcSession = NFCNDEFReaderSession.init(delegate: self, queue: nil, invalidateAfterFirstRead: true)
        nfcSession?.begin()
    }
    @IBAction func ReturnPressed(_ sender: Any) {
       // buttonAction()
        logIn(key: (TextBox.text ?? "n/a"))
    }
    
    
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    print("Volume Button Pressed")
                    NFC()
                   
                }
            }
        }
    }

    private func showPopupMessage1(attributes: EKAttributes, title: String, titleColor: UIColor, description: String, descriptionColor: UIColor, buttonTitleColor: UIColor, buttonBackgroundColor: UIColor, image: UIImage? = nil) {
        
        var themeImage: EKPopUpMessage.ThemeImage?
        
        if let image = image {
            themeImage = .init(image: .init(image: image, size: CGSize(width: 60, height: 60), contentMode: .scaleAspectFit))
        }
        
        let title = EKProperty.LabelContent(text: title, style: .init(font: MainFont.medium.with(size: 24), color: .standardContent, alignment: .center))
        let description = EKProperty.LabelContent(text: description, style: .init(font: MainFont.light.with(size: 16), color: .standardContent, alignment: .center))
        let button = EKProperty.ButtonContent(label: .init(text: "Got it!", style: .init(font: MainFont.bold.with(size: 16), color: .standardContent)), backgroundColor: .standardContent, highlightedBackgroundColor: .standardContent)
        let message = EKPopUpMessage(themeImage: themeImage, title: title, description: description, button: button) {
            SwiftEntryKit.dismiss()
        }
        
        let contentView = EKPopUpMessageView(with: message)
        SwiftEntryKit.display(entry: contentView, using: attributes)
    }
    private func showLightAwesomePopupMessage1(attributes: EKAttributes) {
        let image = UIImage(named: "ic_done_all_light_48pt")!
        let title = "Awesome!"
        let description = "You are using SwiftEntryKit, and this is a pop up with important content"
        showPopupMessage1(attributes: attributes, title: title, titleColor: .white, description: description, descriptionColor: .white, buttonTitleColor: .gray, buttonBackgroundColor: .white, image: image)

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        //SwiftEntryKit.dismiss()
    }
    func updateTotal(){
        
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        
        let  docRef = db.collection("TotalUsage").document("Usage");
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                
                var unlocks = document.data()!["TotalUnlocks"] as! Int
                if(self.totalUpdated){
                    return
                }
                self.totalUpdated = true
                unlocks += 1
                
                docRef.updateData([
                "TotalUnlocks": unlocks,
                ]) { err in
                    if let err = err {
                        print("Error updating document: \(err)")
                    } else {
                        print("Document successfully updated")
                    }
                }
                
            } else {
                print("Document does not exist TU")
            }
        }
    }
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        print("The session was invalidated: \(error.localizedDescription)")
    }
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        var result = ""
        for payload in messages[0].records {
            result += String.init(data: payload.payload.advanced(by: 3), encoding: .utf8)! // 1
        }
        logIn(key: result)
        
    }
    func logIn(key: String){
        DispatchQueue.main.async {
            let db = Firestore.firestore()
            let settings = db.settings
            settings.areTimestampsInSnapshotsEnabled = true
            db.settings = settings
            
            db.collection("users").whereField("Key", isEqualTo: key ?? "n/a")
                .getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        if((querySnapshot?.isEmpty)!){
                            print("Does not Exist")
                            let alert = UIAlertController(title: "Invalid Key", message: "Please enter a valid key to continue.", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                            
                            self.present(alert, animated: true)
                        }
                        for document in querySnapshot!.documents {
                            
                            //print("\(document.documentID) => \(document.data())")
                            let userDefaults = UserDefaults.standard
                            self.storeNumber = document.data()["StoreNumber"]as! Int
                            //print(storeNumber)
                            let tokenCount = document.data()["Tokens"]as! Int
                            
                            userDefaults.set(tokenCount, forKey: "Tokens")
                            //print("Tokens Remaining: \(tokenCount)")
                            var unlocks = document.data()["Unlocks"]as! Int
                            let active = document.data()["Active"]as! Bool
                            
                            if(active){
                                self.updateTotal()
                                userDefaults.set(self.storeNumber, forKey: "StoreNumber")
                                userDefaults.set(document.documentID, forKey: "DocID")
                                let  docRef = db.collection("users").document(document.documentID);
                                unlocks += 1
                                docRef.updateData([
                                    "Unlocks": unlocks
                                ]) { err in
                                    if let err = err {
                                        print("Error updating document: \(err)")
                                        let alert = UIAlertController(title: "Error Sending Unlock Data", message: "Please let Conor know if you see this message. Tell him this: \(document.documentID)", preferredStyle: .alert)
                                        
                                        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                                        
                                        self.present(alert, animated: true)
                                        
                                    } else {
                                        print("Document successfully updated")
                                    }
                                }
                                let docID = userDefaults.string(forKey: "DocID")
                                //print("This is the document ID: \(docID)")
                                if(self.storeNumber == 9999){
                                    clearLog()
                                    var imei = false
                                    
                                    userDefaults.set("000000000000000", forKey: "IMEI")
                                    userDefaults.set("123456789", forKey: "Serial")
                                    userDefaults.set(12345, forKey: "WO")
                                    setModelName()
                                    setTestList()
                                    let model = userDefaults.string(forKey: "model")
                                    
                                    if (model == "iPhone 4" ||
                                        model == "iPhone 4s" ||
                                        model == "iPhone 5" ||
                                        model == "iPhone 5s" ||
                                        model == "iPhone 6"  ||
                                        model == "iPhone SE" ||
                                        model == "iPhone 6 Plus"  ||
                                        model == "iPhone 6s" ||
                                        model == "iPhone 6s Plus" ||
                                        model == "iPhone 7" ||
                                        model == "iPhone 7 Plus" ||
                                        model == "iPhone 8" ||
                                        model == "iPhone 8 Plus" ||
                                        model == "iPhone X" ||
                                        model == "iPhone XS" ||
                                        model == "iPhone XS Max" ||
                                        model == "iPhone XR" ||
                                        model == "iPad Mini Cellular" ||
                                        model == "iPad Mini 2 Cellular" ||
                                        model == "iPad Mini 3 Cellular" ||
                                        model == "iPad Mini 4 Cellular" ||
                                        model == "iPad Pro 9.7 Inch Cellular" ||
                                        model == "iPad Pro 12.9 Inch Cellular" ||
                                        model == "iPad Pro 12.9 Inch 2. Generation Cellular" ||
                                        model == "iPad Pro 10.5 Inch Cellular")
                                        
                                        
                                    {
                                        imei = true
                                        userDefaults.set(imei, forKey: "CellularModel")
                                    }
                                    else{
                                        imei = false
                                        userDefaults.set(imei, forKey: "CellularModel")
                                    }
                                    self.logID()
                                    
                                    
                                    
                                    self.performSegue(withIdentifier: "SegueToDemo", sender: self)
                                }
                                else{
                                   self.performSegue(withIdentifier: "SegueToStart", sender: self)
                                }
                            }
                            else{
                                let alert = UIAlertController(title: "This account is no longer active", message: "Please visit iOSTest.tools to signup or contact info@iostest.tools about any questions", preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                                
                                self.present(alert, animated: true)
                            }
                        }
                    }
            }
        }
    }
    func logID(){
        let userDefaults = UserDefaults.standard
        userDefaults.set(Date(), forKey: "StartTime")
        
        var IMEI: String?
        
        IMEI = userDefaults.string(forKey: "IMEI")
        let WO = userDefaults.string(forKey: "WO")
        print(WO)
        var Model = userDefaults.string(forKey: "model")
        //storeNumber = userDefaults.integer(forKey: "StoreNumber")
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            print(uuid)
            
            let  docRef = db.collection("Devices").document(IMEI!);
            docRef.getDocument { (document, error) in
                if let document = document {
                    //var WO = (document.data()!["Workorders"])
                    //print(WO)
                    
                    if document.exists{
                        print("\(document.documentID) => \(document.data())")
                        docRef.updateData([
                            "UUID": uuid,
                            "Model": Model,
                            "UpdateDate": Date(),
                            "StoreNumber": self.storeNumber,
                            
                            //Nested: "N/A",
                            //NestedDate: Date()
                        ]) { err in
                            if let err = err {
                                print("Error updating document: \(err)")
                                
                            } else {
                                //print("Document successfully updated")
                            }
                        }
                        
                    } else {
                        //var ref: DocumentReference? = nil
                        docRef.setData([
                            "UUID": uuid,
                            "Model": Model,
                            "UpdateDate": Date(),
                            "StoreNumber": self.storeNumber,
                            ]) { err in
                                if let err = err {
                                    print("Error adding document: \(err)")
                                } else {
                                    //print("Document added with ID: \(ref!.documentID)")
                                }
                        }
                        //print("Document does not exist")
                        
                        
                        
                    }
                }
            }
            
            let  docRef1 = db.collection("Devices/\(IMEI!)/Workorders").document(WO!);
            docRef1.getDocument { (document, error) in
                if let document = document {
                    //var WO = (document.data()!["Workorders"])
                    //print(WO)
                    
                    if document.exists{
                        print("\(document.documentID) => \(document.data())")
                        docRef1.updateData([
                            "UUID": uuid,
                            "Model": Model,
                            "PassCount": 0,
                            "FailCount": 0,
                            "Result": "N/A",
                            "IQC": false,
                            "IQCToken": false,
                            "EndTime": "N/A",
                            //Nested: "N/A",
                            //NestedDate: Date()
                        ]) { err in
                            if let err = err {
                                print("Error updating document: \(err)")
                                
                            } else {
                                print("Document successfully updated")
                            }
                        }
                        
                    } else {
                        //var ref: DocumentReference? = nil
                        docRef1.setData([
                            "Result": "N/A",
                            "StartTime": Date(),
                            "PassCount": 0,
                            "FailCount": 0,
                            "IQC": false,
                            "IQCToken": false,
                            "EndTime": "N/A"
                        ]) { err in
                            if let err = err {
                                print("Error adding document: \(err)")
                            } else {
                                //print("Document added with ID: \(ref!.documentID)")
                            }
                        }
                        print("Document does not exist")
                        
                        
                        
                    }
                }
            }
            
        }
    }
}


func getIPAddress() -> String? {
    var address: String?
    var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
    if getifaddrs(&ifaddr) == 0 {
        var ptr = ifaddr
        while ptr != nil {
            defer { ptr = ptr?.pointee.ifa_next }
            
            let interface = ptr?.pointee
            let addrFamily = interface?.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
//                if let name: String = String(cString: (interface?.ifa_name)!), name == "en0" {
//                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
//                    getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
//                    address = String(cString: hostname)
//                }
            }
        }
        freeifaddrs(ifaddr)
    }
    return address
}

