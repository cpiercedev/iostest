//
//  ManualViewController.swift
//  iOSTestTools
//
//  Created by Conor Pierce on 2/2/19.
//  Copyright © 2019 Conor Pierce. All rights reserved.
//

import UIKit

class ManualViewController: UIViewController {

    private let injectedView: UIView
    
    init(with view: UIView) {
        injectedView = view
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = injectedView
    }

}
