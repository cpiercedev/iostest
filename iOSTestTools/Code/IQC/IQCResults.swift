//
//  ResultsViewTable.swift
//
//
//  Created by Conor Pierce on 8/23/18.
//
import Foundation
import UIKit
import Firebase
import BarcodeScanner

class IQCResults: UITableViewController {
    
    
    @IBOutlet var SaveButton: UIButton!
    @IBOutlet var IMEILabel: UILabel!
    @IBOutlet var WOLabel: UILabel!
    // @IBOutlet var BatteryHealthLabel: UILabel!
//    @IBOutlet var ChargingLabel: UILabel!
//    @IBOutlet var MultiTouchLabel: UILabel!
//    @IBOutlet var TouchGridLabel: UILabel!
//    @IBOutlet var Touch3DLabel: UILabel!
    @IBOutlet var LCDLabel: UILabel!
//    @IBOutlet var AudioLoopbackLabel: UILabel!
//
//    @IBOutlet var HeadphoneLabel: UILabel!
//    @IBOutlet var SpeakerLabel: UILabel!
//    @IBOutlet var FrontCameraLabel: UILabel!
//    @IBOutlet var RearCameraLabel: UILabel!
//    @IBOutlet var VibrationLabel: UILabel!
   
    @IBOutlet var AccelLabel: UILabel!
    
    @IBOutlet var IDLabel: UILabel!
    @IBOutlet var BluetoothLabel: UILabel!
    @IBOutlet var NFCLabel: UILabel!
    @IBOutlet var ProxLabel: UILabel!
    @IBOutlet var GyroLabel: UILabel!
    
    @IBOutlet var PassLabel: UILabel!
    
    @IBOutlet var AmbientLabel: UILabel!
    @IBOutlet var BarometerLabel: UILabel!
    
    @IBOutlet var SimLabel: UILabel!
    @IBOutlet var CellularLabel: UILabel!
    @IBOutlet var WifiLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let yellow = UIColor(red: 1, green: 1, blue: 102/255, alpha: 1)
        let red = UIColor(red: 219/255, green: 41/255, blue: 27/255, alpha: 1)
        let green = UIColor(red: 51/255, green: 204/255, blue: 0, alpha: 1)
        
        
        var testResults: [String] = []
        
        
        let userDefaults = UserDefaults.standard
        //sendIQCLog(controller: self)
        
        let IMEI = userDefaults.string(forKey: "IMEI")
        
        let WO = userDefaults.string(forKey: "WO") ?? "N/A"
        userDefaults.set("N/A", forKey: "WO")
        
        let Serial = userDefaults.string(forKey: "Serial")
        
        //        let LCD = userDefaults.string(forKey: "LCD")
        //        testResults.append(LCD ?? "False")
        //
        //        let Touch3D = userDefaults.string(forKey: "3DTouch")
        //        testResults.append(Touch3D ?? "False")
        //
        //        let MultiTouch = userDefaults.string(forKey: "MultiTouch")
        //        testResults.append(Touch3D ?? "False")
        //
        //        let Touch = userDefaults.string(forKey: "TouchGrid")
        //        testResults.append(Touch3D ?? "False")
        //
        //        let Charging = userDefaults.string(forKey: "charging")
        //        testResults.append(Touch3D ?? "False")
        //
        //        let RearCamera = userDefaults.string(forKey: "RearCamera")
        //        testResults.append(Touch3D ?? "False")
        //
        //        let FrontCamera = userDefaults.string(forKey: "FrontCamera")
        //        testResults.append(Touch3D ?? "False")
        //
        //        let Gyro = userDefaults.string(forKey: "Gyroscope")
        //        testResults.append(Touch3D ?? "False")
        //
        //        let Speaker = userDefaults.string(forKey: "Speakers")
        //        let IDAuth = userDefaults.string(forKey: "IDAuth")
        //        let Loopback = userDefaults.string(forKey: "Loopback")
        //        let Vibration = userDefaults.string(forKey: "vibration")
        //        let Prox = userDefaults.string(forKey: "Prox")
        //        let Accel = userDefaults.string(forKey: "Accelerometer")
        //        let Bluetooth = userDefaults.string(forKey: "bluetooth")
        //        //let Battery = userDefaults.string(forKey: "BatteryHealth")
        //        let Headphone = userDefaults.string(forKey: "Headphone")
        //        let NFC = userDefaults.string(forKey: "NFC")
        //        let Barometer = userDefaults.string(forKey: "Barometer")
        //        let Ambient = userDefaults.string(forKey: "Ambient")
        //         let SIM = userDefaults.string(forKey: "SIM")
        //        let Cell = userDefaults.string(forKey: "Cellular")
        //        let Wifi = userDefaults.string(forKey: "Wifi")
        
        let LCD = userDefaults.string(forKey: "LCD")
        testResults.append(LCD ?? "False")
        
        let MultiTouch = userDefaults.string(forKey: "MultiTouch")
        testResults.append(MultiTouch ?? "False")
        
        let Charging = userDefaults.string(forKey: "charging")
        testResults.append(Charging ?? "False")
        
        let RearCamera = userDefaults.string(forKey: "RearCamera")
        testResults.append(RearCamera ?? "False")
        
        let FrontCamera = userDefaults.string(forKey: "FrontCamera")
        testResults.append(FrontCamera ?? "False")
        
        let Gyro = userDefaults.string(forKey: "Gyroscope")
        testResults.append(Gyro ?? "False")
        
        
        let Speaker = userDefaults.string(forKey: "Speakers")
        testResults.append(Speaker ?? "False")
        
        
        let IDAuth = userDefaults.string(forKey: "IDAuth")
        testResults.append(IDAuth ?? "False")
        
        
        let Loopback = userDefaults.string(forKey: "Loopback")
        testResults.append(Loopback ?? "False")
        
        
        let Vibration = userDefaults.string(forKey: "vibration")
        testResults.append(Vibration ?? "False")
        
        
        let Touch3D = userDefaults.string(forKey: "3DTouch")
        testResults.append(Touch3D ?? "False")
        
        let TouchGrid = userDefaults.string(forKey: "TouchGrid")
        testResults.append(TouchGrid ?? "False")
        
        //    let Battery = userDefaults.string(forKey: "BatteryHealth")
        //    testResults.append(Battery ?? "False")
        
        let NFC = userDefaults.string(forKey: "NFC")
        testResults.append(NFC ?? "False")
        
        let Bluetooth = userDefaults.string(forKey: "Bluetooth")
        testResults.append(Bluetooth ?? "False")
        
        let Proximity = userDefaults.string(forKey: "Prox")
        testResults.append(Proximity ?? "False")
        
        let accel = userDefaults.string(forKey: "Accelerometer")
        testResults.append(accel ?? "False")
        
        let Headphone = userDefaults.string(forKey: "Headphone")
        testResults.append(Headphone ?? "False")
        
        let Ambient = userDefaults.string(forKey: "Ambient")
        testResults.append(Ambient ?? "False")
        
        let Barometer = userDefaults.string(forKey: "Barometer")
        testResults.append(Barometer ?? "False")
        
        let WRT = userDefaults.string(forKey: "WRT")
        testResults.append(WRT ?? "False")
        
        let SIM = userDefaults.string(forKey: "SIM")
        testResults.append(SIM ?? "False")
        
        let Wifi = userDefaults.string(forKey: "Wifi")
        testResults.append(Wifi ?? "False")
        
        let Cell = userDefaults.string(forKey: "Cellular")
        testResults.append(Cell ?? "False")
        
        
        var result = true
        for tests in testResults{
            if(tests == "Fail"){
                result = false
            }
        }
        
        if(result){
            PassLabel.text = "Pass"
            PassLabel.textColor = green
        }
        else{
            PassLabel.text = "Fail"
            PassLabel.textColor = red
        }
        //WOLabel.text = WO
        IMEILabel.text = IMEI
        
        if(LCD == "Pass"){
            LCDLabel.text = "Pass"
            LCDLabel.textColor = green
        }
        else if(LCD == "Fail"){
            LCDLabel.text = "Fail"
            LCDLabel.textColor = red
        }
        else{
            LCDLabel.text = "N/A"
            LCDLabel.textColor = yellow
        }
        if(accel == "Pass"){
            AccelLabel.text = "Pass"
            AccelLabel.textColor = green
        }
        else if(accel == "Fail"){
            AccelLabel.text = "Fail"
            AccelLabel.textColor = red
        }
        else{
            AccelLabel.text = "N/A"
            AccelLabel.textColor = yellow
        }
        if(Bluetooth == "Pass"){
            BluetoothLabel.text = "Pass"
            BluetoothLabel.textColor = green
        }
        else if(Bluetooth == "Fail"){
            BluetoothLabel.text = "Fail"
            BluetoothLabel.textColor = red
        }
        else{
            BluetoothLabel.text = "N/A"
            BluetoothLabel.textColor = yellow
        }

        
        //        if(Battery != ""){
        //
        //            //BatteryHealthLabel.text = Battery
        //            //BatteryHealthLabel.textColor = green
        //
        //        }
//        if(NFC == "Pass"){
//            NFCLabel.text = "Pass"
//            NFCLabel.textColor = green
//        }
//        else if(NFC == "Fail"){
//            NFCLabel.text = "Fail"
//            NFCLabel.textColor = red
//        }
//        else{
//            NFCLabel.text = "N/A"
//            NFCLabel.textColor = yellow
//        }
        if(IDAuth == "Pass"){
            IDLabel.text = "Pass"
            IDLabel.textColor = green
        }
        else if(IDAuth == "Fail"){
            IDLabel.text = "Fail"
            IDLabel.textColor = red
        }
        else{
            
            IDLabel.text = "N/A"
            IDLabel.textColor = yellow
        }

        if(Proximity == "Pass"){
            ProxLabel.text = "Pass"
            ProxLabel.textColor = green
        }
        else if(Proximity == "Fail"){
            
            ProxLabel.text = "Fail"
            ProxLabel.textColor = red
        }
        else{
            ProxLabel.text = "N/A"
            ProxLabel.textColor = yellow
        }
        if(Barometer == "Pass"){
            BarometerLabel.text = "Pass"
            BarometerLabel.textColor = green
        }
        else if(Barometer == "Fail"){
            
            BarometerLabel.text = "Fail"
            BarometerLabel.textColor = red
        }
        else{
            BarometerLabel.text = "N/A"
            BarometerLabel.textColor = yellow
        }
        if(SIM == "Pass"){
            SimLabel.text = "Pass"
            SimLabel.textColor = green
        }
        else if(SIM == "Fail"){
            
            SimLabel.text = "Fail"
            SimLabel.textColor = red
        }
        else{
            SimLabel.text = "N/A"
            SimLabel.textColor = yellow
        }
        if(Wifi == "Pass"){
            WifiLabel.text = "Pass"
            WifiLabel.textColor = green
        }
        else if(Wifi == "Fail"){
            
            WifiLabel.text = "Fail"
            WifiLabel.textColor = red
        }
        else{
            WifiLabel.text = "N/A"
            WifiLabel.textColor = yellow
        }
        if(Cell == "Pass"){
            CellularLabel.text = "Pass"
            CellularLabel.textColor = green
        }
        else if(Cell == "Fail"){
            
            CellularLabel.text = "Fail"
            CellularLabel.textColor = red
        }
        else{
            CellularLabel.text = "N/A"
            CellularLabel.textColor = yellow
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    // MARK: - Table view data source
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override func viewDidAppear(_ animated: Bool) {
        let userDefaults = UserDefaults.standard
        //userDefaults.set("N/A", forKey: "WO")
        let WOText = (userDefaults.string(forKey: "WO") ?? "N/A")
        
        WOLabel.text = WOText
        
    }

    
    @IBAction func addWO(_ sender: Any) {
        let viewController = BarcodeScannerViewController()
        viewController.codeDelegate = self
        viewController.errorDelegate = self
        viewController.dismissalDelegate = self
        viewController.headerViewController.titleLabel.text = "Scan barcode"
        viewController.headerViewController.closeButton.tintColor = .red
        viewController.messageViewController.textLabel.text = "Saving log to Workorder"
        present(viewController, animated: true, completion: nil)
    }
    func logID(){
        let userDefaults = UserDefaults.standard
        userDefaults.set(Date(), forKey: "StartTime")
        let imei
            = userDefaults.bool(forKey: "CellularModel")
        var IMEI: String?
        if(imei){
            IMEI = userDefaults.string(forKey: "IMEI")
        }
        else{
            IMEI = userDefaults.string(forKey: "Serial")
        }
        let WO = userDefaults.string(forKey: "WO")
        let storeNumber = (userDefaults.string(forKey: "StoreNumber") ?? "N/A")
        print(WO)
        var Model = userDefaults.string(forKey: "model")
        //storeNumber = userDefaults.integer(forKey: "StoreNumber")
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            print(uuid)
            
            let  docRef = db.collection("Devices").document(IMEI!);
            docRef.getDocument { (document, error) in
                if let document = document {
                    //var WO = (document.data()!["Workorders"])
                    //print(WO)
                    
                    if document.exists{
                        print("\(document.documentID) => \(document.data())")
                        docRef.updateData([
                            "UUID": uuid,
                            "Model": Model,
                            "UpdateDate": Date(),
                            "StoreNumber": storeNumber,
                            
                            //Nested: "N/A",
                            //NestedDate: Date()
                        ]) { err in
                            if let err = err {
                                print("Error updating document: \(err)")
                                
                            } else {
                                //print("Document successfully updated")
                            }
                        }
                        
                    } else {
                        //var ref: DocumentReference? = nil
                        docRef.setData([
                            "UUID": uuid,
                            "Model": Model,
                            "UpdateDate": Date(),
                            "StoreNumber": storeNumber,
                            ]) { err in
                                if let err = err {
                                    print("Error adding document: \(err)")
                                } else {
                                    //print("Document added with ID: \(ref!.documentID)")
                                }
                        }
                        //print("Document does not exist")
                        
                        
                        
                    }
                }
            }
            
            let  docRef1 = db.collection("Devices/\(IMEI!)/Workorders").document(WO!);
            docRef1.getDocument { (document, error) in
                if let document = document {
                    //var WO = (document.data()!["Workorders"])
                    //print(WO)
                    
                    if document.exists{
                        print("\(document.documentID) => \(document.data())")
                        docRef1.updateData([
                            "UUID": uuid,
                            "Model": Model,
                            "PassCount": 0,
                            "FailCount": 0,
                            "Result": "N/A",
                            "IQC": false,
                            "IQCToken": false,
                            "EndTime": "N/A",
                            //Nested: "N/A",
                            //NestedDate: Date()
                        ]) { err in
                            if let err = err {
                                print("Error updating document: \(err)")
                                
                            } else {
                                print("Document successfully updated")
                            }
                        }
                        
                    } else {
                        //var ref: DocumentReference? = nil
                        docRef1.setData([
                            "Result": "N/A",
                            "StartTime": Date(),
                            "PassCount": 0,
                            "FailCount": 0,
                            "IQC": false,
                            "IQCToken": false,
                            "EndTime": "N/A"
                        ]) { err in
                            if let err = err {
                                print("Error adding document: \(err)")
                            } else {
                                //print("Document added with ID: \(ref!.documentID)")
                            }
                        }
                        print("Document does not exist")
                        
                        
                        
                    }
                }
            }
            
        }
    }
}
extension IQCResults: BarcodeScannerCodeDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print(code)
        let userDefaults = UserDefaults.standard
        let WO = code.replacingOccurrences(of: "w", with: "", options: NSString.CompareOptions.literal, range: nil)
        WOLabel.text = WO
        SaveButton.isEnabled = false
        SaveButton.setTitle("Saved", for: .normal)
        
        userDefaults.set(WO, forKey: "WO")
        sendIQCLog(controller: self)
        logID()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            controller.dismiss(animated: true, completion: nil)
        }
        
        //controller.reset()
    }
}
extension IQCResults: BarcodeScannerErrorDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        print(error)
    }
}
extension IQCResults: BarcodeScannerDismissalDelegate {
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
