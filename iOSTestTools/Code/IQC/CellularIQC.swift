//
//  CellularViewController.swift
//  DeviceTesting
//
//  Created by Conor Pierce on 11/1/18.
//  Copyright © 2018 Conor Pierce. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer
import SwiftEntryKit

class CellularIQC: UIViewController {
    @IBOutlet var testIcon: UIImageView!
    @IBOutlet var testLabel: UILabel!
    
    var testName = "Cellular"
    var seguePath = "SegueToWifi"
    
    var reachability: Reachability?
    let hostNames = [nil, "google.com", "invalidhost"]
    var hostIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let volumeView = MPVolumeView(frame: CGRect(x: 0, y: 1000, width: 0, height: 30))
        volumeView.isHidden = false
        volumeView.alpha = 0.01
        
        self.view.addSubview(volumeView)
        //      volumeView.backgroundColor = UIColor.red
        
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged(notification:)),
                                               name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"),
                                               object: nil)
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        checkTestList()
        networkCheck()
    }
    override func viewDidDisappear(_ animated: Bool) {
        stopNotifier()
    }
    func networkCheck(){
        startHost(at: 0)
        
    }
    
    @objc func volumeChanged(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if volumeChangeType == "ExplicitVolumeChange" {
                    // your code goes here
                    if SwiftEntryKit.isCurrentlyDisplaying {
                        return
                    }
                    var attributes: EKAttributes
                    var imageName = "SIMFailed.png"
                    attributes = .bottomFloat
                    attributes.hapticFeedbackType = .success
                    attributes.screenInteraction = .dismiss
                    attributes.entryInteraction = .absorbTouches
                    attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
                    attributes.screenBackground = .color(color: .standardContent)
                    attributes.entryBackground = .color(color: .white)
                    attributes.entranceAnimation = .init(translate: .init(duration: 0.7, spring: .init(damping: 1, initialVelocity: 0)), scale: .init(from: 0.6, to: 1, duration: 0.7), fade: .init(from: 0.8, to: 1, duration: 0.3))
                     attributes.exitAnimation = .init(scale: .init(from: 1, to: 0.7, duration: 0.3), fade: .init(from: 1, to: 0, duration: 0.3))
                    attributes.displayDuration = .infinity
                    attributes.border = .value(color: .black, width: 0.5)
                    attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 5))
                    attributes.statusBar = .dark
                    attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.minEdge), height: .intrinsic)
                    //descriptionString = "Top floating alert view with button bar. Smooths in animately."
                    
                    let title = EKProperty.LabelContent(text: "Mark test manually", style: .init(font: MainFont.medium.with(size: 15), color: .black))
                    let description = EKProperty.LabelContent(text: "If you can't complete the test, you can mark it manually.", style: .init(font: MainFont.light.with(size: 13), color: .black))
                    let image = EKProperty.ImageContent(imageName: imageName, size: CGSize(width: 35, height: 35), contentMode: .scaleAspectFit)
                    let simpleMessage = EKSimpleMessage(image: image, title: title, description: description)
                    
                    // Generate buttons content
                    let buttonFont = MainFont.medium.with(size: 16)
                    
                    // Close button - Just dismiss entry when the button is tapped
                    let closeButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let closeButtonLabel = EKProperty.LabelContent(text: "Not Available", style: closeButtonLabelStyle)
                    let closeButton = EKProperty.ButtonContent(label: closeButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) {
                        self.markNA()
                        SwiftEntryKit.dismiss()
                    }
                    
                    
                    // Ok Button - Make transition to a new entry when the button is tapped
                    let okButtonLabelStyle = EKProperty.LabelStyle(font: buttonFont, color: .standardContent)
                    let okButtonLabel = EKProperty.LabelContent(text: "Failed", style: okButtonLabelStyle)
                    let okButton = EKProperty.ButtonContent(label: okButtonLabel, backgroundColor: .clear, highlightedBackgroundColor:  .standardContent) { [unowned self] in
                        //var attributes = self.dataSource.bottomAlertAttributes
                        self.markFail()
                        SwiftEntryKit.dismiss()
                        
                    }
                    let buttonsBarContent = EKProperty.ButtonBarContent(with: closeButton, okButton, separatorColor: .standardContent, buttonHeight: 60, expandAnimatedly: false)
                    
                    // Generate the content
                    let alertMessage = EKAlertMessage(simpleMessage: simpleMessage, imagePosition: .left, buttonBarContent: buttonsBarContent)
                    
                    let contentView = EKAlertMessageView(with: alertMessage)
                    
                    SwiftEntryKit.display(entry: contentView, using: attributes)
                }
            }
        }
    }
    
    func startHost(at index: Int) {
        stopNotifier()
        setupReachability(hostNames[index], useClosures: true)
        startNotifier()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.startHost(at: (index + 1) % 3)
        }
    }
    
    func setupReachability(_ hostName: String?, useClosures: Bool) {
        let reachability: Reachability?
        if let hostName = hostName {
            reachability = Reachability(hostname: hostName)
            // hostNameLabel.text = hostName
        } else {
            reachability = Reachability()
            //hostNameLabel.text = "No host name"
        }
        self.reachability = reachability
        // print("--- set up with host name: \(hostNameLabel.text!)")
        
        if useClosures {
            reachability?.whenReachable = { reachability in
                self.updateLabelColourWhenReachable(reachability)
            }
            reachability?.whenUnreachable = { reachability in
                self.updateLabelColourWhenNotReachable(reachability)
            }
        } else {
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(reachabilityChanged(_:)),
                name: .reachabilityChanged,
                object: reachability
            )
        }
    }
    
    func startNotifier() {
        //print("--- start notifier")
        do {
            try reachability?.startNotifier()
        } catch {
            // networkStatus.textColor = .red
            // networkStatus.text = "Unable to start\nnotifier"
            return
        }
    }
    
    func stopNotifier() {
        //print("--- stop notifier")
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
        reachability = nil
    }
    
    func updateLabelColourWhenReachable(_ reachability: Reachability) {
        //print("\(reachability.description) - \(reachability.connection)")
        if reachability.connection == .wifi {
            testLabel.text = "Please disconnect Wifi and connect to a Cellular Network, if not available manually mark the test."
        }
        else{
            markPass()
            // detailLabel.text = "Turn Wifi On and connect to a local Network, then return to this screen."
        }
    }
    
    //self.networkStatus.text = "\(reachability.connection)"
    
    func updateLabelColourWhenNotReachable(_ reachability: Reachability) {
        //print("\(reachability.description) - \(reachability.connection)")
        
        // self.networkStatus.textColor = .red
        
        //self.networkStatus.text = "\(reachability.connection)"
    }
    
    @objc func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
        
        if reachability.connection != .none {
            updateLabelColourWhenReachable(reachability)
        } else {
            updateLabelColourWhenNotReachable(reachability)
        }
    }
    
    deinit {
        stopNotifier()
    }
    
    func markPass(){
        testLabel.text = "Passed"
        testIcon.image = UIImage(named: "NetworkEnabled.png")
        print("\(testName) Passed")
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("Pass", forKey: testName)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if(singleTest){
                self.dismiss(animated: true, completion: nil)
                //self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
            else{
                self.performSegue(withIdentifier: self.seguePath, sender: self)
            }
        }
        
    }
    func markNA(){
        sendNA(NAItem: testName)
        let userDefaults = UserDefaults.standard
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        userDefaults.set("N/A", forKey: testName)
        if(singleTest){
            dismiss(animated: true, completion: nil)
            //performSegue(withIdentifier: seguePath, sender: self)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
        
    }
    func markFail(){
        print("\(testName) Failed")
        let userDefaults = UserDefaults.standard
        userDefaults.set("Fail", forKey: testName)
        sendFail(FailItem: testName)
        let singleTest = userDefaults.bool(forKey: "SingleTest")
        
        if(singleTest){
            dismiss(animated: true, completion: nil)
            //performSegue(withIdentifier: seguePath, sender: self)
        }
        else{
            performSegue(withIdentifier: seguePath, sender: self)
        }
    }
    func checkTestList(){
        let userDefaults = UserDefaults.standard
        var testWillRun = false
        let testList = userDefaults.stringArray(forKey: "testList")
        for tests in testList!{
            if( tests == testName){
                testWillRun = true
                break
            }
        }
        
        
        if(!testWillRun){
            print("Test Not available")
            let singleTest = userDefaults.bool(forKey: "SingleTest")
            testLabel.text = "Not Available"
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                if(singleTest){
                    self.dismiss(animated: true, completion: nil)
                    //self.performSegue(withIdentifier: self.seguePath, sender: self)
                }
                else{
                    self.performSegue(withIdentifier: self.seguePath, sender: self)
                }
            }
            
        }
        
    }
}
